﻿Imports System.Data.SqlClient
Imports System.IO

Public Class FormAutoDeleteFile

    Dim DelLogStr As String
    Sub Delwritefile(ByVal MessageStr As String)
        DelLogStr = Now.ToString & " : " & MessageStr & Chr(13) & Chr(10)
        My.Computer.FileSystem.WriteAllText(LogPath & "D" & Format(Now, "yyyyMMdd") & ".log", DelLogStr, True)
    End Sub
    Dim DelErrLogStr As String
    Sub DelwriteErrfile(ByVal MessageStr As String)
        DelErrLogStr = Now.ToString & " : " & MessageStr & Chr(13) & Chr(10)
        My.Computer.FileSystem.WriteAllText(LogPath & "D" & Format(Now, "yyyyMMdd") & "Err.log", DelErrLogStr, True)
    End Sub

    'Dim CompletePath As String = System.Configuration.ConfigurationSettings.AppSettings("CompletePath")
    'Dim LogPath As String = System.Configuration.ConfigurationSettings.AppSettings("LogPath")

    Dim CompletePath As String = ""
    Dim PendingPath As String = ""
    Dim HDUPLOADPath As String = ""
    Dim LogPath As String = ""
    Dim SqlConnString As String = "workstation id=MIKEL;packet size=4096;user id=mam_admin;data source=10.13.220.35;persist security info=False;initial catalog=MAM;password=ptsP@ssw0rd"
    Function ReadSystemConfig(ByVal path As String) As String
        Dim mycommand As New SqlCommand
        Dim sqlconn As New SqlConnection(SqlConnString)
        mycommand.Connection = sqlconn
        Dim dr As SqlDataReader
        Dim sRtn As String = ""
        Dim FSSYSTEM_CONFIG As String = ""

        Try

            sqlconn.Open()
            mycommand.CommandText = "SELECT FSSYSTEM_CONFIG FROM TBSYSTEM_CONFIG "
            dr = mycommand.ExecuteReader()
            If dr.Read = True Then
                FSSYSTEM_CONFIG = dr(0).ToString
                dr.Close()
            End If
            sqlconn.Close()

        Catch ex As Exception

            sqlconn.Close()
            Exit Function
        End Try
        If FSSYSTEM_CONFIG <> "" Then
            Dim xdoc As Xml.XmlDocument = New Xml.XmlDocument
            xdoc.LoadXml(FSSYSTEM_CONFIG)
            Dim node As Xml.XmlNode = xdoc.SelectSingleNode(path)
            If node.InnerText = "" Then
                sRtn = ""
            End If
            sRtn = node.InnerText
        End If

        Return sRtn
    End Function

    Private Sub FormAutoDeleteFile_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Dim mycommand As New SqlCommand
        Dim sqlconn As New SqlConnection(SqlConnString)
        CompletePath = ReadSystemConfig("/ServerConfig/PGM_Config/CompletePath")
        PendingPath = ReadSystemConfig("/ServerConfig/PGM_Config/PendingPath")
        HDUPLOADPath = ReadSystemConfig("/ServerConfig/PGM_Config/PTS_AP_UPLOAD_PATH")

        LogPath = ReadSystemConfig("/ServerConfig/PGM_Config/LogPath")
        Try
            sqlconn.Open()
            mycommand.Connection = sqlconn
            '查詢已審核完成且在14天內要播出的檔案,且未轉檔
            Dim da As SqlDataAdapter = New SqlDataAdapter("SP_Q_GET_PLAY_VIDEOID_LIST", sqlconn)
            Dim dt As New DataTable()
            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.Fill(dt)
            If System.IO.Directory.Exists(CompletePath) = True Then
                Dim oFS As New DirectoryInfo(CompletePath)
                Dim oFile As FileInfo
                For Each oFile In oFS.GetFiles()
                    If oFile.Extension.ToLower = ".mxf" Then
                        Dim flag As Boolean = False
                        For i = 0 To dt.Rows.Count - 1
                            If dt.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6) + ".MXF" = oFile.Name.ToUpper() Then
                                flag = True
                            End If
                        Next
                        If flag = False Then '不在14天的清單內,需要刪除檔案
                            Delwritefile("刪除已審區檔案:" + oFile.Name + "")
                            If System.IO.File.Exists(CompletePath + oFile.Name) = True Then
                                System.IO.File.Delete(CompletePath + oFile.Name)
                            End If
                            If System.IO.File.Exists(CompletePath + oFile.Name + ".ok") = True Then
                                System.IO.File.Delete(CompletePath + oFile.Name + ".ok")
                            End If

                            If System.IO.File.Exists(PendingPath + oFile.Name) = True Then
                                Delwritefile("刪除待審區檔案:" + oFile.Name + "")
                                System.IO.File.Delete(PendingPath + oFile.Name)
                            End If
                            If System.IO.File.Exists(PendingPath + oFile.Name + ".ok") = True Then
                                System.IO.File.Delete(PendingPath + oFile.Name + ".ok")
                            End If


                        End If
                    End If
                Next
            End If

            '取得審核未通過的檔案(審核日期在三天以前到30天以前的區間)
            Dim da1 As SqlDataAdapter = New SqlDataAdapter("SP_Q_GET_NOT_APPROVE_FILE", sqlconn)
            Dim dt1 As New DataTable()
            da1.SelectCommand.CommandType = CommandType.StoredProcedure
            da1.Fill(dt1)
            For i = 0 To dt1.Rows.Count - 1
                If System.IO.File.Exists(HDUPLOADPath + dt1.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6) + ".MXF") = True Then
                    Delwritefile("刪除上傳區檔案:" + dt1.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6) + ".MXF")
                    System.IO.File.Delete(HDUPLOADPath + dt1.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6) + ".MXF")
                End If

                If System.IO.File.Exists(PendingPath + dt1.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6) + ".MXF") = True Then
                    Delwritefile("刪除待審區檔案:" + dt1.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6) + ".MXF" + "")
                    System.IO.File.Delete(PendingPath + dt1.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6) + ".MXF")
                End If
                If System.IO.File.Exists(PendingPath + dt1.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6) + ".MXF" + ".ok") = True Then
                    System.IO.File.Delete(PendingPath + dt1.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6) + ".MXF" + ".ok")
                End If
            Next
            
            sqlconn.Close()
            sqlconn.Dispose()
            GC.Collect()

            'End If
        Catch ex As Exception

            sqlconn.Close()
            DelwriteErrfile("刪除已審區檔案發生錯誤,訊息:" + ex.Message)

        End Try

        Close()
    End Sub
End Class
