﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormTempZoneFileMove
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItemSetting = New System.Windows.Forms.MenuItem()
        Me.MenuItemStop = New System.Windows.Forms.MenuItem()
        Me.MenuItemStart = New System.Windows.Forms.MenuItem()
        Me.MenuItemExit = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.TimerRefresh = New System.Windows.Forms.Timer(Me.components)
        Me.TimerFtp = New System.Windows.Forms.Timer(Me.components)
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.DataGridViewWatchTempZone = New System.Windows.Forms.DataGridView()
        Me.ColumnBroadID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnVID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnSourcePath = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnDescPath = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.DataGridViewWatchTempZone, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemSetting, Me.MenuItemStop, Me.MenuItemStart, Me.MenuItemExit, Me.MenuItem1})
        '
        'MenuItemSetting
        '
        Me.MenuItemSetting.Index = 0
        Me.MenuItemSetting.Text = "設定(&S)"
        '
        'MenuItemStop
        '
        Me.MenuItemStop.Index = 1
        Me.MenuItemStop.Text = "暫停(&P)"
        '
        'MenuItemStart
        '
        Me.MenuItemStart.Enabled = False
        Me.MenuItemStart.Index = 2
        Me.MenuItemStart.Text = "啟動(&G)"
        '
        'MenuItemExit
        '
        Me.MenuItemExit.Index = 3
        Me.MenuItemExit.Text = "離開(&X)"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 4
        Me.MenuItem1.Text = "aa"
        Me.MenuItem1.Visible = False
        '
        'TimerRefresh
        '
        Me.TimerRefresh.Interval = 10000
        '
        'TimerFtp
        '
        Me.TimerFtp.Interval = 5000
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1080, 530)
        Me.TabControl1.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.DataGridViewWatchTempZone)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(1072, 504)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "暫存區檔案狀態"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'DataGridViewWatchTempZone
        '
        Me.DataGridViewWatchTempZone.AllowUserToAddRows = False
        Me.DataGridViewWatchTempZone.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewWatchTempZone.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColumnBroadID, Me.ColumnType, Me.ColumnVID, Me.ColumnSourcePath, Me.ColumnDescPath, Me.ColumnStatus})
        Me.DataGridViewWatchTempZone.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridViewWatchTempZone.Location = New System.Drawing.Point(0, 0)
        Me.DataGridViewWatchTempZone.Name = "DataGridViewWatchTempZone"
        Me.DataGridViewWatchTempZone.RowTemplate.Height = 24
        Me.DataGridViewWatchTempZone.Size = New System.Drawing.Size(1072, 504)
        Me.DataGridViewWatchTempZone.TabIndex = 0
        '
        'ColumnBroadID
        '
        Me.ColumnBroadID.HeaderText = "單號"
        Me.ColumnBroadID.Name = "ColumnBroadID"
        '
        'ColumnType
        '
        Me.ColumnType.HeaderText = "類型"
        Me.ColumnType.Name = "ColumnType"
        '
        'ColumnVID
        '
        Me.ColumnVID.HeaderText = "VideoID"
        Me.ColumnVID.Name = "ColumnVID"
        '
        'ColumnSourcePath
        '
        Me.ColumnSourcePath.HeaderText = "來源路徑"
        Me.ColumnSourcePath.Name = "ColumnSourcePath"
        Me.ColumnSourcePath.Width = 300
        '
        'ColumnDescPath
        '
        Me.ColumnDescPath.HeaderText = "目的位置"
        Me.ColumnDescPath.Name = "ColumnDescPath"
        Me.ColumnDescPath.Width = 300
        '
        'ColumnStatus
        '
        Me.ColumnStatus.HeaderText = "處理狀態"
        Me.ColumnStatus.Name = "ColumnStatus"
        '
        'FormTempZoneFileMove
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1080, 530)
        Me.Controls.Add(Me.TabControl1)
        Me.Menu = Me.MainMenu1
        Me.Name = "FormTempZoneFileMove"
        Me.Text = "暫存區HD檔案處理"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.DataGridViewWatchTempZone, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItemSetting As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemStop As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemStart As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemExit As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents TimerRefresh As System.Windows.Forms.Timer
    Friend WithEvents TimerFtp As System.Windows.Forms.Timer
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents DataGridViewWatchTempZone As System.Windows.Forms.DataGridView
    Friend WithEvents ColumnBroadID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnVID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnSourcePath As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnDescPath As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnStatus As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
