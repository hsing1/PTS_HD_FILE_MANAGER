﻿Imports System.Threading
Imports System.IO
Imports System.Data.SqlClient
Public Class FormTempZoneFileMove
    Dim SqlConnString As String = "workstation id=MIKEL;packet size=4096;user id=mam_admin;data source=10.13.210.33;persist security info=False;initial catalog=MAM;password=ptsP@ssw0rd"


    Dim listClass As New List(Of Class1)
    Dim listThread As New List(Of Thread)
    Dim listString As New List(Of String)
    Dim maxThread As Integer = System.Configuration.ConfigurationSettings.AppSettings("maxThread")

    Public Class Class1
        Public SourceFileName As String
        Public DestFileName As String
        'Public Status As Single
        Public Status As String
        Public ProgressMsg As String
        Public ProgressPercent As Integer
        Public bIsCopyDone As Boolean
        Public videoid As String


        Dim _BufferSize As Integer = 1000000

        Public Event UpdateStatus(ByVal SourceFileName As String, ByVal Status As String)
        Sub FileCpy(ByVal fArry As Object)

            Dim sFileName As String = fArry(0)
            Dim sNewFileName As String = fArry(1)
            Dim input As System.IO.FileStream
            Dim output As System.IO.FileStream

            Dim m_iBytes As Long = 0
            Dim m_aBuffer(_BufferSize) As Byte

            Dim m_totalSize As Long
            Dim m_transferBytes As Long = 0
            Dim startTime As Double = Microsoft.VisualBasic.DateAndTime.Timer
            Dim nowTime As Double

            Try
                If System.IO.File.Exists(sNewFileName + ".ok") Then
                    System.IO.File.Delete(sNewFileName + ".ok")
                End If
                m_totalSize = FileLen(sFileName)
                input = New System.IO.FileStream(sFileName, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                output = New System.IO.FileStream(sNewFileName, System.IO.FileMode.Create)

                m_iBytes = input.Read(m_aBuffer, 0, m_aBuffer.Length)

                nowTime = Microsoft.VisualBasic.DateAndTime.Timer

                Do While (m_iBytes > 0)
                    m_transferBytes += m_iBytes
                    output.Write(m_aBuffer, 0, m_iBytes)

                    If Microsoft.VisualBasic.DateAndTime.Timer - nowTime > 1 Then
                        nowTime = Microsoft.VisualBasic.DateAndTime.Timer
                        ReportProgress(nowTime - startTime, m_transferBytes, m_totalSize)
                    End If

                    m_aBuffer.Clear(m_aBuffer, 0, m_aBuffer.Length)
                    m_iBytes = input.Read(m_aBuffer, 0, m_aBuffer.Length)
                Loop
                input.Close()
                output.Close()
                System.IO.File.Create(sNewFileName + ".ok")
            Catch ex As Exception
                bIsCopyDone = True

                'Status = ex.Message
                'Throw ex
            End Try


            bIsCopyDone = True
        End Sub


        Sub FileCopy(ByVal fArry As Object)

            Dim sFileName As String = fArry(0)
            Dim sNewFileName As String = fArry(1)


            Try
                If System.IO.File.Exists(sNewFileName) Then
                    System.IO.File.Delete(sNewFileName)
                End If
                If System.IO.File.Exists(sNewFileName + ".ok") Then
                    System.IO.File.Delete(sNewFileName + ".ok")
                End If
                Status = "拷貝中...."
                System.IO.File.Copy(sFileName, sNewFileName)
                Dim SourceFileSize As Long
                Dim DescFileSize As Long

                SourceFileSize = FileLen(sFileName)
                DescFileSize = FileLen(sNewFileName)
                If SourceFileSize <> DescFileSize Then

                End If
                bIsCopyDone = True
                System.IO.File.Create(sNewFileName + ".ok")
                RaiseEvent UpdateStatus(sFileName, "拷貝完成")
                'GC.Collect()
                'GC.WaitForPendingFinalizers()

            Catch ex As Exception
                bIsCopyDone = True
                RaiseEvent UpdateStatus(sFileName, "拷貝失敗,錯誤訊息:" + ex.Message)
                'Status = ex.Message
                'Throw ex
            End Try
            
        End Sub
        Public Sub ReportProgress(ByVal spendTime As Double, ByVal transferBytes As Long, ByVal totalSize As Long)
            'Status = Format(transferBytes * 100 / totalSize, "00.0") & "%"
            Status = Int(transferBytes * 100 / totalSize).ToString
        End Sub

        'Private Sub onCopyInProgress(ByVal Sender As CopyEx.CopyEngine, ByVal e As CopyEx.CopyEngine.CopyEventArgs)
        '    Try
        '        Dim dtrate As Double = e.CurrentTauxTransfert
        '        If dtrate.ToString(".##") >= 0 Then
        '            'Status = "進度" & e.CurrentPercent.ToString(".##") & "%"
        '            Status = e.CurrentPercent
        '        End If
        '    Catch ex As Exception
        '    End Try

        'End Sub
    End Class

    Public Class ClassPENDING
        Public SourceFileName As String
        Public DestFileName As String
        Public Status As String
        Public bIsCopyDone As Boolean
        Public videoid As String
        Dim _BufferSize As Integer = 1000000
        Public Event UpdatePENDingStatus(ByVal SourceFileName As String, ByVal Status As String)

        Sub FileCopy(ByVal fArry As Object)

            Dim sFileName As String = fArry(0)
            Dim sNewFileName As String = fArry(1)
            Try
                If System.IO.File.Exists(sNewFileName) Then
                    System.IO.File.Delete(sNewFileName)
                End If
                'If System.IO.File.Exists(sNewFileName + ".ok") Then
                '    System.IO.File.Delete(sNewFileName + ".ok")
                'End If
                Status = "拷貝中...."
                Try
                    System.IO.File.Move(sFileName, sNewFileName)
                    If System.IO.File.Exists(sNewFileName) = False Then
                        RaiseEvent UpdatePENDingStatus(sFileName, "搬移檔案失敗")
                    End If
                Catch ex As Exception
                    System.IO.File.Copy(sFileName, sNewFileName)
                    Dim SourceFileSize As Long
                    Dim DescFileSize As Long

                    SourceFileSize = FileLen(sFileName)
                    DescFileSize = FileLen(sNewFileName)
                    If SourceFileSize <> DescFileSize Then
                        RaiseEvent UpdatePENDingStatus(sFileName, "複製檔案失敗")
                    End If
                End Try

                
                bIsCopyDone = True

                If System.IO.File.Exists(sFileName + ".ok") Then
                    System.IO.File.Delete(sFileName + ".ok")
                End If
                'System.IO.File.Create(sNewFileName + ".ok")
                RaiseEvent UpdatePENDingStatus(sFileName, "作業完成")
                'GC.Collect()
                'GC.WaitForPendingFinalizers()

            Catch ex As Exception
                bIsCopyDone = True
                RaiseEvent UpdatePENDingStatus(sFileName, "作業失敗,錯誤訊息:" + ex.Message)
                'Status = ex.Message
                'Throw ex
            End Try

        End Sub
        Public Sub ReportProgress(ByVal spendTime As Double, ByVal transferBytes As Long, ByVal totalSize As Long)
            'Status = Format(transferBytes * 100 / totalSize, "00.0") & "%"
            Status = Int(transferBytes * 100 / totalSize).ToString
        End Sub
    End Class
    Public Class ClassGPFS
        Public SourceFileName As String
        Public DestFileName As String
        Public Status As String
        Public bIsCopyDone As Boolean
        Public videoid As String
        Dim _BufferSize As Integer = 1000000
        Public Event UpdateGpfsStatus(ByVal SourceFileName As String, ByVal Status As String)

        Sub FileCopy(ByVal fArry As Object)

            Dim sFileName As String = fArry(0)
            Dim sNewFileName As String = fArry(1)
            Try
                '要先去檢查檔案是否在磁帶櫃上,如果磁帶櫃上則要先下指令將檔案由磁帶櫃搬回硬碟
                If System.IO.File.Exists(sNewFileName) Then
                    System.IO.File.Delete(sNewFileName)
                End If
                If System.IO.File.Exists(sNewFileName + ".ok") Then
                    System.IO.File.Delete(sNewFileName + ".ok")
                End If
                Status = "拷貝中...."
                System.IO.File.Copy(sFileName, sNewFileName)
                Dim SourceFileSize As Long
                Dim DescFileSize As Long

                SourceFileSize = FileLen(sFileName)
                DescFileSize = FileLen(sNewFileName)
                If SourceFileSize <> DescFileSize Then
                    RaiseEvent UpdateGpfsStatus(sFileName, "拷貝失敗,來源檔案與目的檔案大小不符")
                End If
                bIsCopyDone = True
                'System.IO.File.Create(sNewFileName + ".ok")
                RaiseEvent UpdateGpfsStatus(sFileName, "拷貝完成")
                'GC.Collect()
                'GC.WaitForPendingFinalizers()

            Catch ex As Exception
                bIsCopyDone = True
                RaiseEvent UpdateGpfsStatus(sFileName, "拷貝失敗,錯誤訊息:" + ex.Message)
                'Status = ex.Message
                'Throw ex
            End Try

        End Sub
        Public Sub ReportProgress(ByVal spendTime As Double, ByVal transferBytes As Long, ByVal totalSize As Long)
            'Status = Format(transferBytes * 100 / totalSize, "00.0") & "%"
            Status = Int(transferBytes * 100 / totalSize).ToString
        End Sub
    End Class

    'Dim TempPath As String = System.Configuration.ConfigurationSettings.AppSettings("TempPath")
    'Dim PendingPath As String = System.Configuration.ConfigurationSettings.AppSettings("PendingPath")
    'Dim CompletePath As String = System.Configuration.ConfigurationSettings.AppSettings("CompletePath")
    'Dim GpfsPath As String = System.Configuration.ConfigurationSettings.AppSettings("GpfsPath")
    'Dim TSMPath As String = System.Configuration.ConfigurationSettings.AppSettings("TSMPath")
    'Dim LogPath As String = System.Configuration.ConfigurationSettings.AppSettings("LogPath")
    Dim TempPath As String = ""
    Dim PendingPath As String = ""
    Dim CompletePath As String = ""
    Dim GpfsPath As String = ""
    Dim TSMPath As String = ""
    Dim LogPath As String = ""
    Dim LouthXmlPath As String = ""

    Dim PendingListClass As New List(Of ClassPENDING)
    Dim PendingListThread As New List(Of Thread)
    Dim GpfsListClass As New List(Of ClassGPFS)
    Dim GpfsListThread As New List(Of Thread)

    Dim LogStr As String
    Sub writefile(ByVal MessageStr As String)
        'My.Computer.FileSystem.WriteAllText(LogPath & Format(Now, "yyyyMMdd") & ".log", Now.ToString & " : " & MessageStr & Chr(13) & Chr(10), True)
        LogStr = LogStr & Now.ToString & " : " & MessageStr & Chr(13) & Chr(10)
        'If TimerWriteFile.Enabled = False Then
        '    TimerWriteFile.Enabled = True
        'End If
    End Sub
    Dim ErrLogStr As String
    Sub writeErrfile(ByVal MessageStr As String)
        'My.Computer.FileSystem.WriteAllText(LogPath & Format(Now, "yyyyMMdd") & ".log", Now.ToString & " : " & MessageStr & Chr(13) & Chr(10), True)
        ErrLogStr = ErrLogStr & Now.ToString & " : " & MessageStr & Chr(13) & Chr(10)
        'If TimerWriteFile.Enabled = False Then
        '    TimerWriteFile.Enabled = True
        'End If
    End Sub

    Dim PendingLogStr As String
    Sub Pendingwritefile(ByVal MessageStr As String)
        PendingLogStr = PendingLogStr & Now.ToString & " : " & MessageStr & Chr(13) & Chr(10)
    End Sub
    Dim PendingErrLogStr As String
    Sub PendingwriteErrfile(ByVal MessageStr As String)
        PendingErrLogStr = PendingErrLogStr & Now.ToString & " : " & MessageStr & Chr(13) & Chr(10)
    End Sub
    Dim GPFSLogStr As String
    Sub GPFSwritefile(ByVal MessageStr As String)
        GPFSLogStr = GPFSLogStr & Now.ToString & " : " & MessageStr & Chr(13) & Chr(10)
    End Sub
    Dim GPFSErrLogStr As String
    Sub GPFSwriteErrfile(ByVal MessageStr As String)
        GPFSErrLogStr = GPFSErrLogStr & Now.ToString & " : " & MessageStr & Chr(13) & Chr(10)
    End Sub

    Dim DelLogStr As String
    Sub Delwritefile(ByVal MessageStr As String)
        DelLogStr = DelLogStr & Now.ToString & " : " & MessageStr & Chr(13) & Chr(10)
    End Sub
    Dim DelErrLogStr As String
    Sub DelwriteErrfile(ByVal MessageStr As String)
        DelErrLogStr = DelErrLogStr & Now.ToString & " : " & MessageStr & Chr(13) & Chr(10)
    End Sub

    Dim LouthDBLogStr As String
    Sub LouthDBwritefile(ByVal MessageStr As String)
        LouthDBLogStr = LouthDBLogStr & Now.ToString & " : " & MessageStr & Chr(13) & Chr(10)
    End Sub
    Dim LouthDBErrLogStr As String
    Sub LouthDBwriteErrfile(ByVal MessageStr As String)
        LouthDBErrLogStr = LouthDBErrLogStr & Now.ToString & " : " & MessageStr & Chr(13) & Chr(10)
    End Sub


    Function GetMinPlayTime(ByVal VideoID As String) As String
        If VideoID.Length = 6 Then
            VideoID = "00" + VideoID
        End If
        Dim mycommandTemp As New SqlCommand
        Dim sqlconnTemp As New SqlConnection(SqlConnString)
        mycommandTemp.Connection = sqlconnTemp
        Dim dr As SqlDataReader
        Dim sRtn As String = ""
        Dim FSSYSTEM_CONFIG As String = ""

        Try
            Dim SQLTXT As String
            SQLTXT = "select CONVERT(char(10),fddate,20) fddate,fsplay_time from tbpgm_combine_queue where fsvideo_id='" + VideoID + "' and fddate >= getdate()-1 union all "
            SQLTXT = SQLTXT + "select CONVERT(char(10),fddate,20) fddate,fsplaytime fsplay_time from tbpgm_arr_promo where fsvideo_id='" + VideoID + "' and fddate >= getdate()-1 order by fddate,fsplay_time"
            sqlconnTemp.Open()
            mycommandTemp.CommandText = SQLTXT


            dr = mycommandTemp.ExecuteReader()
            If dr.Read = True Then
                sRtn = dr(0).ToString + " " + dr(1).ToString
                dr.Close()
            End If
            sqlconnTemp.Close()

        Catch ex As Exception

            sqlconnTemp.Close()
            sRtn = "2000-01-01 00:00:00:00"
            Return sRtn
            Exit Function
        End Try


        Return sRtn
    End Function

    Private Sub TimerRefresh_Tick(sender As System.Object, e As System.EventArgs) Handles TimerRefresh.Tick
        TimerRefresh.Enabled = False
        Dim mycommand As New SqlCommand
        Dim sqlconn As New SqlConnection(SqlConnString)
        Dim i As Integer
        Try

            sqlconn.Open()
            mycommand.Connection = sqlconn
            Dim da As SqlDataAdapter = New SqlDataAdapter("SP_Q_GET_MOVE_PENDING_LIST_MIX", sqlconn)
            Dim dt As New DataTable()
            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.Fill(dt)

            Dim j As Integer
            Dim k As Integer
            Dim flag As Boolean

            For i = dt.Rows.Count - 1 To 0 Step -1
                flag = False
                For k = DataGridViewWatchTempZone.RowCount - 1 To 0 Step -1
                    If DataGridViewWatchTempZone.Rows(k).Cells(4).Value = dt.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6) Then
                        flag = True '已經加入過不用重新加入
                    End If
                Next
                If flag = False Then
                    DataGridViewWatchTempZone.RowCount = DataGridViewWatchTempZone.RowCount + 1
                    j = DataGridViewWatchTempZone.RowCount - 1
                    DataGridViewWatchTempZone.Rows(j).Cells(0).Value = "尚未開始"
                    DataGridViewWatchTempZone.Rows(j).Cells(1).Value = dt.Rows(i)("FSBRO_ID").ToString()
                    DataGridViewWatchTempZone.Rows(j).Cells(2).Value = "UPLOAD"
                    DataGridViewWatchTempZone.Rows(j).Cells(3).Value = dt.Rows(i)("FSFILE_NO").ToString()
                    DataGridViewWatchTempZone.Rows(j).Cells(4).Value = dt.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6)
                    DataGridViewWatchTempZone.Rows(j).Cells(5).Value = TempPath + dt.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6) + ".mxf"
                    DataGridViewWatchTempZone.Rows(j).Cells(6).Value = PendingPath + dt.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6) + ".mxf"
                    DataGridViewWatchTempZone.Rows(j).Cells(7).Value = GetMinPlayTime(dt.Rows(i)("FSVIDEO_ID").ToString())
                End If
            Next
            sqlconn.Close()
            sqlconn.Dispose()
            'GC.Collect()
            'GC.WaitForPendingFinalizers()
        Catch ex As Exception
            writeErrfile("更新取得要搬移到[待審區]清單時發生錯誤,訊息:" & ex.Message.ToString())
            sqlconn.Close()
            'Exit Sub
        End Try
        TimerRefresh.Enabled = True
    End Sub

    Private Sub FormTempZoneFileMove_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        TimerRefresh.Enabled = True
        TimerUpload.Enabled = True
        TimerPENDINGRefresh.Enabled = True
        TimerPendingUpload.Enabled = True
        TimerGPFSRefresh.Enabled = True
        TimerGPFSUpload.Enabled = True
        TimerWriteLog.Enabled = True
        TimerGPFSCheckTSMFileStatus.Enabled = True
        TimerGCClear.Enabled = True


        TempPath = ReadSystemConfig("/ServerConfig/PGM_Config/PTS_AP_UPLOAD_PATH")

        PendingPath = ReadSystemConfig("/ServerConfig/PGM_Config/PendingPath")
        CompletePath = ReadSystemConfig("/ServerConfig/PGM_Config/CompletePath")
        GpfsPath = ReadSystemConfig("/ServerConfig/PGM_Config/GpfsPath")
        TSMPath = ReadSystemConfig("/ServerConfig/PGM_Config/TSMPath")
        LogPath = ReadSystemConfig("/ServerConfig/PGM_Config/LogPath")
        LouthXmlPath = ReadSystemConfig("/ServerConfig/PGM_Config/LouthXmlPath")

        writefile("開啟程式")

        
            DataGridViewWatchTempZone.RowHeadersWidth = 20

            Dim L As Integer

            For L = 0 To maxThread - 1

                listClass.Add(New Class1)
                listThread.Add(New Thread(AddressOf listClass(L).FileCopy))
                'listString.Add(SiteName)

                'MaxThreadCount = MaxThreadCount + 1
            Next

            For L = 0 To maxThread - 1

                PendingListClass.Add(New ClassPENDING)
                PendingListThread.Add(New Thread(AddressOf PendingListClass(L).FileCopy))
                'listString.Add(SiteName)

                'MaxThreadCount = MaxThreadCount + 1
            Next

            For L = 0 To maxThread - 1

                GpfsListClass.Add(New ClassGPFS)
                GpfsListThread.Add(New Thread(AddressOf GpfsListClass(L).FileCopy))
                'listString.Add(SiteName)

                'MaxThreadCount = MaxThreadCount + 1
            Next
    End Sub

    Private Sub MenuItemStop_Click(sender As System.Object, e As System.EventArgs) Handles MenuItemStop.Click
        TimerRefresh.Enabled = False
        TimerUpload.Enabled = False
        TimerPENDINGRefresh.Enabled = False
        TimerPendingUpload.Enabled = False
        TimerGPFSRefresh.Enabled = False
        TimerGPFSUpload.Enabled = False
        TimerWriteLog.Enabled = False
        TimerGPFSCheckTSMFileStatus.Enabled = False

        MenuItemStop.Enabled = False
        MenuItemStart.Enabled = True


    End Sub

    Private Sub MenuItemStart_Click(sender As System.Object, e As System.EventArgs) Handles MenuItemStart.Click
        TimerRefresh.Enabled = True
        TimerUpload.Enabled = True
        TimerPENDINGRefresh.Enabled = True
        TimerPendingUpload.Enabled = True
        TimerGPFSRefresh.Enabled = True
        TimerGPFSUpload.Enabled = True
        TimerWriteLog.Enabled = True
        TimerGPFSCheckTSMFileStatus.Enabled = True
        MenuItemStop.Enabled = True
        MenuItemStart.Enabled = False
    End Sub

    Private Sub TimerUpload_Tick(sender As System.Object, e As System.EventArgs) Handles TimerUpload.Tick
        If DataGridViewWatchTempZone.RowCount = 0 Then
            Exit Sub

        End If
        Dim i As Integer
        Dim FileNO As String = ""
        TimerUpload.Enabled = False
        Dim workRow As Integer = -1
        Dim MinTime As String = ""

        Try
            For i = DataGridViewWatchTempZone.RowCount - 1 To 0 Step -1
                If (DataGridViewWatchTempZone.Rows(i).Cells(0).Value = "尚未開始") Then
                    If (MinTime = "") Or (MinTime > DataGridViewWatchTempZone.Rows(i).Cells(7).Value) Then
                        workRow = i
                        MinTime = DataGridViewWatchTempZone.Rows(i).Cells(7).Value
                    End If
                ElseIf (Mid(DataGridViewWatchTempZone.Rows(i).Cells(0).Value, 1, 4) = "拷貝失敗") Then
                    DataGridViewWatchTempZone.Rows(i).Cells(0).Value = "尚未開始"

                End If
            Next
        
            i = workRow

            If workRow <> -1 Then
                Dim HasCopy As Boolean = False
                If System.IO.File.Exists(DataGridViewWatchTempZone.Rows(i).Cells(5).Value) = True Then
                    FileNO = DataGridViewWatchTempZone.Rows(i).Cells(5).Value
                    Dim fArry As New ArrayList
                    fArry.Add(DataGridViewWatchTempZone.Rows(i).Cells(5).Value)
                    fArry.Add(DataGridViewWatchTempZone.Rows(i).Cells(6).Value)
                    For L = 0 To maxThread - 1

                        If (listThread(L).IsAlive = False) And (HasCopy = False) Then
                            HasCopy = True
                            writefile("開始搬移檔案:" + DataGridViewWatchTempZone.Rows(i).Cells(5).Value + ":到待審區")
                            listClass(L) = New Class1

                            listThread(L).Abort()
                            listThread(L) = Nothing
                            listThread(L) = New Thread(AddressOf listClass(L).FileCopy)
                            listThread(L).Priority = ThreadPriority.BelowNormal
                            listThread(L).IsBackground = True


                            listClass(L).bIsCopyDone = False
                            listClass(L).videoid = DataGridViewWatchTempZone.Rows(i).Cells(4).Value
                            listThread(L).Start(fArry)
                            DataGridViewWatchTempZone.Rows(i).Cells(0).Value = "正在上傳"
                            AddHandler listClass(L).UpdateStatus, AddressOf UpdateStatus


                        End If

                    Next
                End If
            End If
                  
              
            'GC.Collect()
            'GC.WaitForPendingFinalizers()
        Catch ex As Exception
            writeErrfile("搬移檔案:" + FileNO + ":到待審區失敗,訊息:" & ex.Message.ToString())
        End Try
        TimerUpload.Enabled = True

    End Sub


    Public Sub UpdateStatus(ByVal SourceFileName As String, ByVal Status As String)
        Dim WorkVideoID As String = ""
        Try
            Dim k As Integer

            For k = DataGridViewWatchTempZone.RowCount - 1 To 0 Step -1
                If DataGridViewWatchTempZone.Rows(k).Cells(5).Value = SourceFileName Then

                    DataGridViewWatchTempZone.Rows(k).Cells(0).Value = Status
                    WorkVideoID = DataGridViewWatchTempZone.Rows(k).Cells(4).Value
                    If Status = "拷貝完成" Then
                        writefile("檔案:" + DataGridViewWatchTempZone.Rows(k).Cells(5).Value + ":上傳完成")
                        If System.IO.File.Exists(DataGridViewWatchTempZone.Rows(k).Cells(6).Value + ".ok") = True Then
                            If UpdateTBLOG_VIDEO_HD_MC_Status(DataGridViewWatchTempZone.Rows(k).Cells(3).Value, "N", "程式") = False Then
                                writeErrfile("修改[" & WorkVideoID & "]的TBLOG_VIDEO_HD_MC的Status狀態為'N'時失敗")
                                DataGridViewWatchTempZone.Rows(k).Cells(0).Value = "尚未開始"
                            Else
                                DataGridViewWatchTempZone.Rows.RemoveAt(k)
                            End If

                            'Dim sqlcmd As New SqlCommand
                            'sqlcmd.CommandText = "SP_U_TBLOG_VIDEO_HD_MC_BY_FILE_NO"
                            'writefile("開始將" + DataGridViewWatchTempZone.Rows(k).Cells(5).Value + ":的TBLOG_VIDEO_HD_MC的狀態修改成N")
                            'Dim tran As SqlTransaction

                            'Dim sqlconn1 As New SqlConnection(SqlConnString)
                            'sqlconn1.Open()
                            'sqlcmd.Connection = sqlconn1
                            'tran = sqlconn1.BeginTransaction()
                            'sqlcmd.Transaction = tran

                            'sqlcmd.Parameters.Clear()
                            'Dim para1 As SqlParameter
                            'para1 = New SqlParameter("@FSFILE_NO", DataGridViewWatchTempZone.Rows(k).Cells(3).Value)
                            'sqlcmd.Parameters.Add(para1)

                            'Dim para2 As SqlParameter
                            'para2 = New SqlParameter("@FCSTATUS", "N")
                            'sqlcmd.Parameters.Add(para2)

                            'Dim para3 As SqlParameter
                            'para3 = New SqlParameter("@FSUPDAYE_BY", "程式")
                            'sqlcmd.Parameters.Add(para3)

                            'sqlcmd.CommandType = CommandType.StoredProcedure

                            'If (sqlcmd.ExecuteNonQuery() = 0) Then
                            '    tran.Rollback()
                            '    writeErrfile("修改[" & WorkVideoID & "]狀態失敗")
                            '    DataGridViewWatchTempZone.Rows(k).Cells(0).Value = "尚未開始"
                            'Else
                            '    tran.Commit()
                            '    DataGridViewWatchTempZone.Rows.RemoveAt(k)
                            'End If
                            'sqlconn1.Close()
                            'sqlconn1.Dispose()
                            'GC.Collect()

                        End If
                    End If

                End If
            Next
           


        Catch ex As Exception
            For k = DataGridViewWatchTempZone.RowCount - 1 To 0 Step -1
                If DataGridViewWatchTempZone.Rows(k).Cells(5).Value = SourceFileName Then
                    writeErrfile("修改[" & WorkVideoID & "]狀態失敗:訊息:" & ex.Message)
                    DataGridViewWatchTempZone.Rows(k).Cells(0).Value = "尚未開始"
                End If
            Next


        End Try
    End Sub


    Public Sub UpdatePENDingStatus(ByVal SourceFileName As String, ByVal Status As String)
        Dim WorkVideoID As String = ""
        Try
            Dim k As Integer
            For k = DataGridViewPENDINGList.RowCount - 1 To 0 Step -1
                If DataGridViewPENDINGList.Rows(k).Cells(5).Value = SourceFileName Then

                    DataGridViewPENDINGList.Rows(k).Cells(0).Value = Status
                    WorkVideoID = DataGridViewPENDINGList.Rows(k).Cells(4).Value

                    If Status = "作業完成" Then
                        DataGridViewPENDINGList.Rows.RemoveAt(k)
                        AddInsertLouthDBFile(WorkVideoID)
                    End If

                End If
            Next

        Catch ex As Exception
            PendingwriteErrfile("修改[" & WorkVideoID & "]狀態失敗:訊息:" & ex.Message)
        End Try
    End Sub

    Public Sub UpdateGpfsStatus(ByVal SourceFileName As String, ByVal Status As String)
        Dim WorkVideoID As String = ""
        Try
            Dim k As Integer
            For k = DataGridViewGPFS.RowCount - 1 To 0 Step -1
                If DataGridViewGPFS.Rows(k).Cells(5).Value = SourceFileName Then

                    DataGridViewGPFS.Rows(k).Cells(0).Value = Status
                    WorkVideoID = DataGridViewGPFS.Rows(k).Cells(4).Value
                    Dim FileNo As String
                    FileNo = DataGridViewGPFS.Rows(k).Cells(3).Value
                    If Status = "拷貝完成" Then

                        If (DataGridViewGPFS.Rows(k).Cells(2).Value = "Pending") Then '需要修改TBLOG_VIDEO_HD_MC狀態為N
                            GPFSwritefile("檔案:" + DataGridViewGPFS.Rows(k).Cells(5).Value + ":拷貝到待審區完成,開始將狀態改為'N'尚未審核")
                            System.IO.File.Create(DataGridViewGPFS.Rows(k).Cells(6).Value + ".ok")
                            If UpdateTBLOG_VIDEO_HD_MC_Status(FileNo, "N", "程式") = False Then

                            End If
                        Else
                            GPFSwritefile("檔案:" + DataGridViewGPFS.Rows(k).Cells(5).Value + ":拷貝到待播區完成")
                            AddInsertLouthDBFile(WorkVideoID)
                        End If
                        DataGridViewGPFS.Rows.RemoveAt(k) '如果沒有修改狀態成功,移除掉清單則會再重頭開始一次

                    End If


                End If

            Next

        Catch ex As Exception
            GPFSwriteErrfile("修改[" & WorkVideoID & "]狀態失敗:訊息:" & ex.Message)
        End Try
    End Sub

    Function UpdateTBLOG_VIDEO_HD_MC_Status(ByVal FileNO As String, ByVal Status As String, ByVal FSUPDAYE_BY As String) As Boolean
        Try
            Dim sqlcmd As New SqlCommand
            sqlcmd.CommandText = "SP_U_TBLOG_VIDEO_HD_MC_BY_FILE_NO"
            writefile("開始將" + FileNO + ":的TBLOG_VIDEO_HD_MC的狀態修改成" + Status)
            Dim tran As SqlTransaction

            Dim sqlconn1 As New SqlConnection(SqlConnString)
            sqlconn1.Open()
            sqlcmd.Connection = sqlconn1
            tran = sqlconn1.BeginTransaction()
            sqlcmd.Transaction = tran

            sqlcmd.Parameters.Clear()
            Dim para1 As SqlParameter
            para1 = New SqlParameter("@FSFILE_NO", FileNO)
            sqlcmd.Parameters.Add(para1)

            Dim para2 As SqlParameter
            para2 = New SqlParameter("@FCSTATUS", Status)
            sqlcmd.Parameters.Add(para2)

            Dim para3 As SqlParameter
            para3 = New SqlParameter("@FSUPDAYE_BY", FSUPDAYE_BY)
            sqlcmd.Parameters.Add(para3)

            sqlcmd.CommandType = CommandType.StoredProcedure

            If (sqlcmd.ExecuteNonQuery() = 0) Then
                tran.Rollback()
                writeErrfile("修改[" & FileNO & ":的TBLOG_VIDEO_HD_MC資料失敗")
                sqlconn1.Close()
                sqlconn1.Dispose()
                'GC.Collect()
                Return False
            Else
                tran.Commit()
                sqlconn1.Close()
                sqlconn1.Dispose()
                'GC.Collect()
                Return True
            End If
        Catch ex As Exception
            writeErrfile("修改[" & FileNO & ":的TBLOG_VIDEO_HD_MC資料失敗,訊息:" & ex.Message())
        End Try
        



    End Function


    Private Sub TimerCopy_Tick(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub TimerPENDINGRefresh_Tick(sender As System.Object, e As System.EventArgs) Handles TimerPENDINGRefresh.Tick
        TimerPENDINGRefresh.Enabled = False
        Dim mycommand As New SqlCommand
        Dim sqlconn As New SqlConnection(SqlConnString)
        Dim i As Integer
        Try

            sqlconn.Open()
            mycommand.Connection = sqlconn
            '查詢已審核完成且在14天內要播出的檔案,且未轉檔
            Dim da As SqlDataAdapter = New SqlDataAdapter("SP_Q_GET_CHECK_COMPLETE_LIST_MIX", sqlconn)
            Dim dt As New DataTable()

            'Dim para As SqlParameter=New SqlParameter("@FDDATE", TemArrPromoList.FSPROG_ID);

            'da.SelectCommand.Parameters.Add(para);
            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.Fill(dt)

            'If dt.Rows.Count > 0 Then
            'distinct A.FSPROG_ID as FSID,A.FNEPISODE,D.FSFILE_NO,D.FSVIDEO_ID,B.FCFILE_STATUS,C.FCCHECK_STATUS,D.FCSTATUS
            Dim j As Integer
            Dim k As Integer
            Dim flag As Boolean

            For i = dt.Rows.Count - 1 To 0 Step -1
                If System.IO.File.Exists(CompletePath + dt.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6) + ".mxf") = False Then
                    flag = False
                    For k = DataGridViewPENDINGList.RowCount - 1 To 0 Step -1
                        If DataGridViewPENDINGList.Rows(k).Cells(4).Value = dt.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6) Then
                            flag = True '已經加入過不用重新加入
                        End If
                    Next
                    If flag = False Then
                        DataGridViewPENDINGList.RowCount = DataGridViewPENDINGList.RowCount + 1
                        j = DataGridViewPENDINGList.RowCount - 1
                        DataGridViewPENDINGList.Rows(j).Cells(0).Value = "尚未開始"
                        DataGridViewPENDINGList.Rows(j).Cells(1).Value = dt.Rows(i)("FSID").ToString()
                        DataGridViewPENDINGList.Rows(j).Cells(2).Value = "UPLOAD"
                        DataGridViewPENDINGList.Rows(j).Cells(3).Value = dt.Rows(i)("FSFILE_NO").ToString()
                        DataGridViewPENDINGList.Rows(j).Cells(4).Value = dt.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6)
                        DataGridViewPENDINGList.Rows(j).Cells(5).Value = PendingPath + dt.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6) + ".mxf"
                        DataGridViewPENDINGList.Rows(j).Cells(6).Value = CompletePath + dt.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6) + ".mxf"
                        DataGridViewPENDINGList.Rows(j).Cells(7).Value = GetMinPlayTime(dt.Rows(i)("FSVIDEO_ID").ToString())
                    End If

                    'Else 'If System.IO.File.Exists(CompletePath + dt.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6) + ".mxf") = False
                    '    For k = DataGridViewPENDINGList.RowCount - 1 To 0 Step -1
                    '        If DataGridViewPENDINGList.Rows(k).Cells(4).Value = dt.Rows(i)("FSVIDEO_ID").ToString().Substring(2, 6) Then
                    '            flag = True '已經加入過不用重新加入
                    '            If DataGridViewPENDINGList.Rows(k).Cells(0).Value = "尚未開始" Then
                    '                DataGridViewPENDINGList.Rows.RemoveAt(k)
                    '            End If
                    '        End If
                    '    Next
                End If
            Next
            sqlconn.Close()
            sqlconn.Dispose()
            'GC.Collect()
            'GC.WaitForPendingFinalizers()
            'End If
        Catch ex As Exception
            writeErrfile("更新取得要搬移到[待播區]清單時發生錯誤,訊息:" & ex.Message.ToString())
            sqlconn.Close()
            'Exit Sub
        End Try
        TimerPENDINGRefresh.Enabled = True
    End Sub

    Private Sub TimerPendingUpload_Tick(sender As System.Object, e As System.EventArgs) Handles TimerPendingUpload.Tick
        If DataGridViewPENDINGList.RowCount = 0 Then
            Exit Sub

        End If
        Dim i As Integer
        Dim FileNo As String = ""

        Dim workRow As Integer = -1
        Dim MinTime As String = ""

        Try
            For i = DataGridViewPENDINGList.RowCount - 1 To 0 Step -1
                If (DataGridViewPENDINGList.Rows(i).Cells(0).Value = "尚未開始") Then
                    If (MinTime = "") Or (MinTime > DataGridViewPENDINGList.Rows(i).Cells(7).Value) Then
                        workRow = i
                        MinTime = DataGridViewPENDINGList.Rows(i).Cells(7).Value
                    End If
                ElseIf (Mid(DataGridViewPENDINGList.Rows(i).Cells(0).Value, 1, 4) = "作業失敗") Or (Mid(DataGridViewPENDINGList.Rows(i).Cells(0).Value, 1, 6) = "搬移檔案失敗") Or (Mid(DataGridViewPENDINGList.Rows(i).Cells(0).Value, 1, 6) = "複製檔案失敗") Then
                    DataGridViewPENDINGList.Rows(i).Cells(0).Value = "尚未開始"
                End If
            Next

            i = workRow

            TimerPendingUpload.Enabled = False
            If workRow <> -1 Then
                FileNo = DataGridViewPENDINGList.Rows(i).Cells(5).Value
                Dim HasCopy As Boolean = False
                If System.IO.File.Exists(DataGridViewPENDINGList.Rows(i).Cells(5).Value) = True Then

                    Dim fArry As New ArrayList
                    fArry.Add(DataGridViewPENDINGList.Rows(i).Cells(5).Value)
                    fArry.Add(DataGridViewPENDINGList.Rows(i).Cells(6).Value)
                    For L = 0 To maxThread - 1

                        If (PendingListThread(L).IsAlive = False) And (HasCopy = False) Then
                            HasCopy = True
                            Pendingwritefile("開始搬移檔案:" + DataGridViewPENDINGList.Rows(i).Cells(5).Value + ":到待審區")
                            PendingListClass(L) = New ClassPENDING

                            PendingListThread(L).Abort()
                            PendingListThread(L) = Nothing
                            PendingListThread(L) = New Thread(AddressOf PendingListClass(L).FileCopy)
                            PendingListThread(L).Priority = ThreadPriority.BelowNormal
                            PendingListThread(L).IsBackground = True


                            PendingListClass(L).bIsCopyDone = False
                            PendingListClass(L).videoid = DataGridViewPENDINGList.Rows(i).Cells(4).Value
                            PendingListThread(L).Start(fArry)
                            DataGridViewPENDINGList.Rows(i).Cells(0).Value = "正在上傳"
                            AddHandler PendingListClass(L).UpdatePENDingStatus, AddressOf UpdatePENDingStatus
                        End If

                    Next
                ElseIf System.IO.File.Exists(DataGridViewPENDINGList.Rows(i).Cells(6).Value) = True Then
                    '來源檔案沒有友存在但目的檔案存在
                    '要移除此資料
                    Pendingwritefile("目的檔案已存在要移除:" + DataGridViewPENDINGList.Rows(i).Cells(5).Value + ":的工作資料")
                    DataGridViewPENDINGList.Rows.RemoveAt(i)
                End If
            End If


         
            'GC.Collect()
            'GC.WaitForPendingFinalizers()
        Catch ex As Exception
            PendingwriteErrfile("搬移檔案:" + FileNo + ":到待播區失敗,訊息:" & ex.Message.ToString())
        End Try

        TimerPendingUpload.Enabled = True

    End Sub


    Private Sub TimerWriteLog_Tick(sender As System.Object, e As System.EventArgs) Handles TimerWriteLog.Tick
        TimerWriteLog.Enabled = False

        Try
            If LogStr <> "" Then
                Try
                    My.Computer.FileSystem.WriteAllText(LogPath & Format(Now, "yyyyMMdd") & ".log", LogStr, True)
                Catch ex As Exception
                    'writeErrfile("寫入訊息失敗" & ex.Message)
                End Try
                LogStr = ""
                'TimerWriteFile.Enabled = False
            End If
            If ErrLogStr <> "" Then
                Try
                    My.Computer.FileSystem.WriteAllText(LogPath & Format(Now, "yyyyMMdd") & "Err.log", ErrLogStr, True)
                Catch ex As Exception

                End Try
                ErrLogStr = ""
                'TimerWriteFile.Enabled = False
            End If
            If PendingLogStr <> "" Then
                Try
                    My.Computer.FileSystem.WriteAllText(LogPath & "P" & Format(Now, "yyyyMMdd") & ".log", PendingLogStr, True)
                Catch ex As Exception
                End Try
                PendingLogStr = ""
            End If
            If PendingErrLogStr <> "" Then
                Try
                    My.Computer.FileSystem.WriteAllText(LogPath & "P" & Format(Now, "yyyyMMdd") & "Err.log", PendingErrLogStr, True)
                Catch ex As Exception
                End Try
                PendingErrLogStr = ""
            End If

            If GPFSLogStr <> "" Then
                Try
                    My.Computer.FileSystem.WriteAllText(LogPath & "G" & Format(Now, "yyyyMMdd") & ".log", GPFSLogStr, True)
                Catch ex As Exception
                End Try
                GPFSLogStr = ""
            End If
            If GPFSErrLogStr <> "" Then
                Try
                    My.Computer.FileSystem.WriteAllText(LogPath & "G" & Format(Now, "yyyyMMdd") & "Err.log", GPFSErrLogStr, True)
                Catch ex As Exception
                End Try
                GPFSErrLogStr = ""
            End If

            If DelLogStr <> "" Then
                Try
                    My.Computer.FileSystem.WriteAllText(LogPath & "D" & Format(Now, "yyyyMMdd") & ".log", DelLogStr, True)
                Catch ex As Exception
                End Try
                DelLogStr = ""
            End If
            If DelErrLogStr <> "" Then
                Try
                    My.Computer.FileSystem.WriteAllText(LogPath & "D" & Format(Now, "yyyyMMdd") & "Err.log", DelErrLogStr, True)
                Catch ex As Exception
                End Try
                DelErrLogStr = ""
            End If

            If LouthDBLogStr <> "" Then
                Try
                    My.Computer.FileSystem.WriteAllText(LogPath & "Louth" & Format(Now, "yyyyMMdd") & ".log", LouthDBLogStr, True)
                Catch ex As Exception
                End Try
                LouthDBLogStr = ""
            End If
            If LouthDBErrLogStr <> "" Then
                Try
                    My.Computer.FileSystem.WriteAllText(LogPath & "Louth" & Format(Now, "yyyyMMdd") & "Err.log", LouthDBErrLogStr, True)
                Catch ex As Exception
                End Try
                LouthDBErrLogStr = ""
            End If

            'GC.Collect()
            'GC.WaitForPendingFinalizers()
        Catch ex As Exception

        End Try
        TimerWriteLog.Enabled = True
    End Sub


    Function ReadSystemConfig(ByVal path As String) As String
        Dim mycommand As New SqlCommand
        Dim sqlconn As New SqlConnection(SqlConnString)
        mycommand.Connection = sqlconn
        Dim dr As SqlDataReader
        Dim sRtn As String = ""
        Dim FSSYSTEM_CONFIG As String = ""

        Try

            sqlconn.Open()
            mycommand.CommandText = "SELECT FSSYSTEM_CONFIG FROM TBSYSTEM_CONFIG "
            dr = mycommand.ExecuteReader()
            If dr.Read = True Then
                FSSYSTEM_CONFIG = dr(0).ToString
                dr.Close()
            End If
            sqlconn.Close()
           
        Catch ex As Exception

            sqlconn.Close()
            Exit Function
        End Try
        If FSSYSTEM_CONFIG <> "" Then
            Dim xdoc As Xml.XmlDocument = New Xml.XmlDocument
            xdoc.LoadXml(FSSYSTEM_CONFIG)
            Dim node As Xml.XmlNode = xdoc.SelectSingleNode(path)
            If node.InnerText = "" Then
                sRtn = ""
            End If
            sRtn = node.InnerText
        End If
       
        Return sRtn
    End Function

    Private Sub TimerGPFSRefresh_Tick(sender As System.Object, e As System.EventArgs) Handles TimerGPFSRefresh.Tick
        TimerGPFSRefresh.Enabled = False
        Dim mycommand As New SqlCommand
        Dim sqlconn As New SqlConnection(SqlConnString)
        Dim i As Integer
        Try

            sqlconn.Open()
            mycommand.Connection = sqlconn
            '查詢已審核完成且在14天內要播出的檔案,且未轉檔
            Dim da As SqlDataAdapter = New SqlDataAdapter("SP_Q_GET_IN_TAPE_LIBRARY_LIST_MIX", sqlconn)
            Dim dt As New DataTable()

            'Dim para As SqlParameter=New SqlParameter("@FDDATE", TemArrPromoList.FSPROG_ID);

            'da.SelectCommand.Parameters.Add(para);
            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.Fill(dt)

            'If dt.Rows.Count > 0 Then
            'distinct A.FSPROG_ID as FSID,A.FNEPISODE,D.FSFILE_NO,D.FSVIDEO_ID,B.FCFILE_STATUS,C.FCCHECK_STATUS,D.FCSTATUS
            Dim j As Integer
            Dim k As Integer
            Dim flag As Boolean

            For i = dt.Rows.Count - 1 To 0 Step -1
                '如果待播區沒有檔案且待審區也沒有檔案才要處理
                'If (System.IO.File.Exists(CompletePath + dt.Rows(i)("FSVIDEO_PROG").ToString().Substring(2, 6) + ".mxf") = False) And (System.IO.File.Exists(PendingPath + dt.Rows(i)("FSVIDEO_PROG").ToString().Substring(2, 6) + ".mxf.ok") = False) Then
                If (System.IO.File.Exists(CompletePath + dt.Rows(i)("FSVIDEO_PROG").ToString().Substring(2, 6) + ".mxf") = False) Then
                    flag = False

                    If (dt.Rows(i)("FCSTATUS").ToString() = "O") Then '審核完成,加入清單中複製到待播區

                        For k = DataGridViewGPFS.RowCount - 1 To 0 Step -1
                            If DataGridViewGPFS.Rows(k).Cells(4).Value = dt.Rows(i)("FSVIDEO_PROG").ToString().Substring(2, 6) Then
                                flag = True '已經加入過不用重新加入
                            End If
                        Next
                        If flag = False Then
                            DataGridViewGPFS.RowCount = DataGridViewGPFS.RowCount + 1
                            j = DataGridViewGPFS.RowCount - 1
                            DataGridViewGPFS.Rows(j).Cells(0).Value = "尚未開始"
                            DataGridViewGPFS.Rows(j).Cells(1).Value = dt.Rows(i)("FSID").ToString()
                            DataGridViewGPFS.Rows(j).Cells(2).Value = "UPLOAD"
                            DataGridViewGPFS.Rows(j).Cells(3).Value = dt.Rows(i)("FSFILE_NO").ToString()
                            DataGridViewGPFS.Rows(j).Cells(4).Value = dt.Rows(i)("FSVIDEO_PROG").ToString().Substring(2, 6)
                            DataGridViewGPFS.Rows(j).Cells(5).Value = dt.Rows(i)("FSFILE_PATH_H").ToString()
                            'DataGridViewGPFS.Rows(j).Cells(5).Value = GpfsPath + dt.Rows(i)("FSID").ToString().Substring(0, 4) + "\" + dt.Rows(i)("FSID").ToString() + "\" + dt.Rows(i)("FSFILE_NO").ToString() + ".mxf"
                            DataGridViewGPFS.Rows(j).Cells(6).Value = CompletePath + dt.Rows(i)("FSVIDEO_PROG").ToString().Substring(2, 6) + ".mxf"
                            DataGridViewGPFS.Rows(j).Cells(7).Value = GetMinPlayTime(dt.Rows(i)("FSVIDEO_PROG").ToString())
                        End If
                    Else '加入清單中,複製檔案到待審區
                        '先在TBLOG_VIDEO_HD_MC中加入資料

                        If (dt.Rows(i)("FSCREATED_BY").ToString() = "") Then '沒有TBLOG_VIDEO_HD_MC資料,要新增資料
                            Dim sqlcmd As New SqlCommand
                            sqlcmd.CommandText = "SP_I_TBLOG_VIDEO_HD_MC"
                            writefile("開始新增" + dt.Rows(i)("FSFILE_NO").ToString() + ":的TBLOG_VIDEO_HD_MC資料")
                            Dim tran As SqlTransaction



                            Dim sqlconn1 As New SqlConnection(SqlConnString)
                            sqlconn1.Open()
                            sqlcmd.Connection = sqlconn1
                            tran = sqlconn1.BeginTransaction()
                            sqlcmd.Transaction = tran

                            sqlcmd.Parameters.Clear()
                            Dim para1 As SqlParameter
                            para1 = New SqlParameter("@FSFILE_NO", dt.Rows(i)("FSFILE_NO").ToString())
                            sqlcmd.Parameters.Add(para1)

                            Dim para2 As SqlParameter
                            para2 = New SqlParameter("@FSBRO_ID", dt.Rows(i)("FSBRO_ID").ToString())
                            sqlcmd.Parameters.Add(para2)

                            Dim para3 As SqlParameter
                            para3 = New SqlParameter("@FSVIDEO_ID", dt.Rows(i)("FSVIDEO_PROG").ToString())
                            sqlcmd.Parameters.Add(para3)

                            Dim para4 As SqlParameter
                            para4 = New SqlParameter("@FSCREATED_BY", "程式")
                            sqlcmd.Parameters.Add(para4)
                            sqlcmd.CommandType = CommandType.StoredProcedure

                            If (sqlcmd.ExecuteNonQuery() = 0) Then
                                tran.Rollback()
                                writeErrfile("新增[" & dt.Rows(i)("FSFILE_NO").ToString() & ":的TBLOG_VIDEO_HD_MC資料失敗")
                                flag = True
                            Else
                                tran.Commit()

                            End If
                            sqlconn1.Close()
                            sqlconn1.Dispose()
                            'GC.Collect()
                        ElseIf (dt.Rows(i)("FCSTATUS").ToString() <> "") Then
                            If UpdateTBLOG_VIDEO_HD_MC_Status(dt.Rows(i)("FSFILE_NO").ToString(), "", "程式") = False Then
                                flag = True
                            End If
                        Else

                        End If

                        'Need get file from GPFS is no file in Review folder 
                        If (System.IO.File.Exists(PendingPath + dt.Rows(i)("FSVIDEO_PROG").ToString().Substring(2, 6) + ".mxf.ok") = False) Then
                            For k = DataGridViewGPFS.RowCount - 1 To 0 Step -1
                                If DataGridViewGPFS.Rows(k).Cells(4).Value = dt.Rows(i)("FSVIDEO_PROG").ToString().Substring(2, 6) Then
                                    flag = True '已經加入過不用重新加入
                                End If
                            Next

                            If flag = False Then
                                DataGridViewGPFS.RowCount = DataGridViewGPFS.RowCount + 1
                                j = DataGridViewGPFS.RowCount - 1
                                DataGridViewGPFS.Rows(j).Cells(0).Value = "尚未開始"
                                DataGridViewGPFS.Rows(j).Cells(1).Value = dt.Rows(i)("FSID").ToString()
                                DataGridViewGPFS.Rows(j).Cells(2).Value = "Pending"
                                DataGridViewGPFS.Rows(j).Cells(3).Value = dt.Rows(i)("FSFILE_NO").ToString()
                                DataGridViewGPFS.Rows(j).Cells(4).Value = dt.Rows(i)("FSVIDEO_PROG").ToString().Substring(2, 6)
                                DataGridViewGPFS.Rows(j).Cells(5).Value = dt.Rows(i)("FSFILE_PATH_H").ToString()
                                'DataGridViewGPFS.Rows(j).Cells(5).Value = GpfsPath + dt.Rows(i)("FSID").ToString().Substring(0, 4) + "\" + dt.Rows(i)("FSID").ToString() + "\" + dt.Rows(i)("FSFILE_NO").ToString() + ".mxf"
                                DataGridViewGPFS.Rows(j).Cells(6).Value = PendingPath + dt.Rows(i)("FSVIDEO_PROG").ToString().Substring(2, 6) + ".mxf"
                                DataGridViewGPFS.Rows(j).Cells(7).Value = GetMinPlayTime(dt.Rows(i)("FSVIDEO_PROG").ToString())
                            End If
                        End If
                    End If

                End If
            Next
            sqlconn.Close()
            sqlconn.Dispose()
            'GC.Collect()
            'GC.WaitForPendingFinalizers()

            'End If
        Catch ex As Exception
            writeErrfile("更新取得GPFS中要搬移到[待播區]或[待審區]清單時發生錯誤,訊息:" & ex.Message.ToString())
            sqlconn.Close()
            'Exit Sub
        End Try
        TimerGPFSRefresh.Enabled = True
    End Sub

    Private Sub TimerGPFSUpload_Tick(sender As System.Object, e As System.EventArgs) Handles TimerGPFSUpload.Tick
        If DataGridViewGPFS.RowCount = 0 Then
            Exit Sub

        End If
        Dim i As Integer
        Dim FileNo As String = ""
        TimerGPFSUpload.Enabled = False


        Dim workRow As Integer = -1
        Dim MinTime As String = ""

        Try
            For i = DataGridViewGPFS.RowCount - 1 To 0 Step -1
                If (DataGridViewGPFS.Rows(i).Cells(0).Value = "可以開始複製") Then
                    If (MinTime = "") Or (MinTime > DataGridViewGPFS.Rows(i).Cells(7).Value) Then
                        workRow = i
                        MinTime = DataGridViewGPFS.Rows(i).Cells(7).Value
                    End If
                ElseIf (Mid(DataGridViewGPFS.Rows(i).Cells(0).Value, 1, 4) = "拷貝失敗") Then
                    DataGridViewGPFS.Rows(i).Cells(0).Value = "尚未開始"

                End If
            Next

            i = workRow
            If workRow <> -1 Then
                Try
                    Dim HasCopy As Boolean = False

                    Dim fArry As New ArrayList
                    FileNo = DataGridViewGPFS.Rows(i).Cells(5).Value
                    fArry.Add(DataGridViewGPFS.Rows(i).Cells(5).Value)
                    fArry.Add(DataGridViewGPFS.Rows(i).Cells(6).Value)
                    For L = 0 To maxThread - 1

                        If (GpfsListThread(L).IsAlive = False) And (HasCopy = False) Then
                            '在正式拷貝之前還是要再次去取得狀態一次,以避免因為前面排程太久的等待時間而檔案又被搬移到磁帶
                            Dim TSMFilePath As String
                            'TSMFilePath = TSMPath
                            TSMFilePath = "/" + DataGridViewGPFS.Rows(i).Cells(5).Value.ToString().Split("\")(4).ToString() + "/" + DataGridViewGPFS.Rows(i).Cells(5).Value.ToString().Split("\")(5).ToString() + "/"

                            ' DataGridViewGPFS.Rows(i).Cells(3).Value.ToString()="G201500300010001"
                            If DataGridViewGPFS.Rows(i).Cells(3).Value.ToString().Substring(0, 1) = "G" Then '節目路徑
                                TSMFilePath = TSMFilePath + DataGridViewGPFS.Rows(i).Cells(3).Value.ToString().Substring(1, 4) + "/"
                                TSMFilePath = TSMFilePath + DataGridViewGPFS.Rows(i).Cells(3).Value.ToString().Substring(1, 7) + "/"
                                TSMFilePath = TSMFilePath + DataGridViewGPFS.Rows(i).Cells(3).Value.ToString() + ".mxf"

                            Else 'promo路徑
                                TSMFilePath = TSMFilePath + DataGridViewGPFS.Rows(i).Cells(3).Value.ToString().Substring(1, 4) + "/"
                                TSMFilePath = TSMFilePath + DataGridViewGPFS.Rows(i).Cells(3).Value.ToString().Substring(1, 11) + "/"
                                TSMFilePath = TSMFilePath + DataGridViewGPFS.Rows(i).Cells(3).Value.ToString() + ".mxf"

                            End If

                            Dim aaa As New WSTSM_GetFileInfo.ServiceTSM
                            Dim TSMretuen As String
                            TSMretuen = aaa.GetFileStatusByTsmPath(TSMFilePath)
                            If TSMretuen = "r" Or TSMretuen = "p" Or TSMretuen = "0" Then '檔案在Local

                                HasCopy = True
                                If DataGridViewGPFS.Rows(i).Cells(2).Value = "UPLOAD" Then
                                    GPFSwritefile("開始複製檔案:" + DataGridViewGPFS.Rows(i).Cells(5).Value + ":到待播區")
                                Else
                                    GPFSwritefile("開始複製檔案:" + DataGridViewGPFS.Rows(i).Cells(5).Value + ":到待審區")
                                End If
                                GpfsListClass(L) = New ClassGPFS

                                GpfsListThread(L).Abort()
                                GpfsListThread(L) = Nothing
                                GpfsListThread(L) = New Thread(AddressOf GpfsListClass(L).FileCopy)
                                GpfsListThread(L).Priority = ThreadPriority.BelowNormal
                                GpfsListThread(L).IsBackground = True


                                GpfsListClass(L).bIsCopyDone = False
                                GpfsListClass(L).videoid = DataGridViewGPFS.Rows(i).Cells(4).Value
                                GpfsListThread(L).Start(fArry)
                                DataGridViewGPFS.Rows(i).Cells(0).Value = "正在上傳"
                                AddHandler GpfsListClass(L).UpdateGpfsStatus, AddressOf UpdateGpfsStatus


                            Else '如果沒有在local則將狀態改為"尚未開始"讓他再去作TSM Recall
                                DataGridViewGPFS.Rows(i).Cells(0).Value = "尚未開始"
                            End If
                            aaa.Abort()
                            aaa = Nothing



                        End If

                    Next

                Catch ex As Exception
                    GPFSwriteErrfile("拷貝檔案:" + FileNo + ":到待播區失敗,訊息:" & ex.Message.ToString())
                End Try
             
            End If

            'GC.Collect()
            'GC.WaitForPendingFinalizers()
        Catch ex As Exception
            GPFSwriteErrfile("檢查拷貝檔案動作失敗,訊息:" & ex.Message.ToString())
        End Try

        TimerGPFSUpload.Enabled = True
    End Sub


  
    Private Sub TimerGPFSCheckTSMFileStatus_Tick(sender As System.Object, e As System.EventArgs) Handles TimerGPFSCheckTSMFileStatus.Tick
        If DataGridViewGPFS.RowCount = 0 Then
            Exit Sub

        End If
        Dim i As Integer
        Dim FileNo As String = ""
        TimerGPFSCheckTSMFileStatus.Enabled = False
        Try
            For i = DataGridViewGPFS.RowCount - 1 To 0 Step -1
                FileNo = DataGridViewGPFS.Rows(i).Cells(5).Value
                'Dim HasCopy As Boolean = False

                'Dim fArry As New ArrayList
                'fArry.Add(DataGridViewGPFS.Rows(i).Cells(5).Value)
                'fArry.Add(DataGridViewGPFS.Rows(i).Cells(6).Value)
                'For L = 0 To maxThread - 1
                '    If (GpfsListThread(L).IsAlive = False) And (HasCopy = False) Then
                '        HasCopy = True
                '    End If

                'Next

             
                '開始將檔案轉為TSM路徑
                Dim TSMFilePath As String
                'TSMFilePath = TSMPath
                TSMFilePath = "/" + DataGridViewGPFS.Rows(i).Cells(5).Value.ToString().Split("\")(4).ToString() + "/" + DataGridViewGPFS.Rows(i).Cells(5).Value.ToString().Split("\")(5).ToString() + "/"
                ' DataGridViewGPFS.Rows(i).Cells(3).Value.ToString()="G201500300010001"
                If DataGridViewGPFS.Rows(i).Cells(3).Value.ToString().Substring(0, 1) = "G" Then '節目路徑
                    TSMFilePath = TSMFilePath + DataGridViewGPFS.Rows(i).Cells(3).Value.ToString().Substring(1, 4) + "/"
                    TSMFilePath = TSMFilePath + DataGridViewGPFS.Rows(i).Cells(3).Value.ToString().Substring(1, 7) + "/"
                    TSMFilePath = TSMFilePath + DataGridViewGPFS.Rows(i).Cells(3).Value.ToString() + ".mxf"

                Else 'promo路徑
                    TSMFilePath = TSMFilePath + DataGridViewGPFS.Rows(i).Cells(3).Value.ToString().Substring(1, 4) + "/"
                    TSMFilePath = TSMFilePath + DataGridViewGPFS.Rows(i).Cells(3).Value.ToString().Substring(1, 11) + "/"
                    TSMFilePath = TSMFilePath + DataGridViewGPFS.Rows(i).Cells(3).Value.ToString() + ".mxf"

                End If


                'TSMPath = "/ptstest/ptstv/HVideo/2015/2015003/G201500300010001.mxf"
                If (DataGridViewGPFS.Rows(i).Cells(0).Value = "尚未開始") Or
                   (DataGridViewGPFS.Rows(i).Cells(0).Value = "磁帶不在架上") Or
                   (DataGridViewGPFS.Rows(i).Cells(0).Value = "沒有找到檔案") Or
                   (DataGridViewGPFS.Rows(i).Cells(0).Value = "呼叫TSM動作發生錯誤") Or
                   (DataGridViewGPFS.Rows(i).Cells(0).Value = "TSM回傳錯誤訊息") Then

                    '呼叫PTS_MAM3.Web-->WS-->WSTSM-->fnServiceTSM.cs 中的 fnGetFileStatusByTsmls 取得檔案狀態
                    '呼叫PTS_MAM3.Web-->WS-->WSTSM-->WSTSM_GetFileInfo.asmx 中的 GetFileStatusByTsmPath 取得檔案狀態,這裡會呼叫PTS_MAM3.Web-->WS-->WSTSM-->fnServiceTSM.cs 中的 fnGetFileStatusByTsmls

                    '呼叫PTS_MAM3.Web-->WS-->WSTSM-->WSTSM.asmx 中的RegisterRecallServiceByTsmPath 註冊要Recall的檔案
                    'public bool RegisterRecallServiceByTsmPath(string tsmRealPath, string notifyAddr, out long jobID)

                    '如果回傳值為"r" or "p" 則可以進行檔案拷貝
                    'DSMLS_RETURN_VALUE fnGetFileStatusByTsmls(string tsmRealPath)

                    ' bool fnRegisterRecallFile(string tsmPath, TSMRecallService.TsmRecallStatus status, string notifyAddr, out long jobID)
                    '如果不在Local則要呼叫TSMRecall
                    GPFSwritefile("開始取得檔案:" + DataGridViewGPFS.Rows(i).Cells(5).Value + ":在TSM狀態")
                    Dim aaa As New WSTSM_GetFileInfo.ServiceTSM
                    Dim TSMretuen As String

                    TSMretuen = aaa.GetFileStatusByTsmPath(TSMFilePath)

                    '                    [System.Runtime.Serialization.EnumMemberAttribute()]
                    'NearLine = 0,

                    '[System.Runtime.Serialization.EnumMemberAttribute()]
                    'TapeOnly = 1,

                    '[System.Runtime.Serialization.EnumMemberAttribute()]
                    'TapeOffline = 2,

                    '[System.Runtime.Serialization.EnumMemberAttribute()]
                    'NotAvailable = 3,

                    '[System.Runtime.Serialization.EnumMemberAttribute()]
                    'SysError = 4,

                    If TSMretuen = "0" Then
                        DataGridViewGPFS.Rows(i).Cells(0).Value = "可以開始複製"
                        GPFSwritefile("檔案:" + DataGridViewGPFS.Rows(i).Cells(5).Value + ":在TSM狀態為[" + TSMretuen + "]可以開始複製")
                    ElseIf TSMretuen = "1" Then
                        GPFSwritefile("檔案:" + DataGridViewGPFS.Rows(i).Cells(5).Value + ":在TSM狀態為[" + TSMretuen + "]需要呼叫TSM-ReCall")
                        Dim bbb As New ServiceTSM.ServiceTSM
                        Dim jobid As Long
                        bbb.RegisterRecallServiceByTsmPath(TSMFilePath, "", jobid)
                        DataGridViewGPFS.Rows(i).Cells(0).Value = "呼叫TSM-ReCall" + ",JOBID=" + jobid.ToString
                        GPFSwritefile("檔案:" + DataGridViewGPFS.Rows(i).Cells(5).Value + ":呼叫TSM-ReCall" + ",JOBID=" + jobid.ToString)
                        bbb.Abort()
                        bbb = Nothing
                    ElseIf TSMretuen = "2" Then
                        DataGridViewGPFS.Rows(i).Cells(0).Value = "磁帶不在架上"
                    ElseIf TSMretuen = "3" Then
                        DataGridViewGPFS.Rows(i).Cells(0).Value = "沒有找到檔案"
                    ElseIf TSMretuen = "4" Then
                        DataGridViewGPFS.Rows(i).Cells(0).Value = "TSM回傳錯誤訊息"
                    Else
                        DataGridViewGPFS.Rows(i).Cells(0).Value = "呼叫TSM動作發生錯誤"
                    End If
                    aaa.Abort()
                    aaa = Nothing

                End If
                If Mid(DataGridViewGPFS.Rows(i).Cells(0).Value, 1, 12) = "呼叫TSM-ReCall" Then
                    Dim aaa As New WSTSM_GetFileInfo.ServiceTSM
                    Dim TSMretuen As String
                    TSMretuen = aaa.GetFileStatusByTsmPath(TSMFilePath)
                    If TSMretuen = "r" Or TSMretuen = "p" Or TSMretuen = "0" Then
                        DataGridViewGPFS.Rows(i).Cells(0).Value = "可以開始複製"
                        GPFSwritefile("檔案:" + DataGridViewGPFS.Rows(i).Cells(5).Value + ":在TSM狀態為[" + TSMretuen + "]可以開始複製")
                    End If
                    aaa.Abort()
                    aaa = Nothing
                End If
            Next
            'GC.Collect()
            'GC.WaitForPendingFinalizers()
        Catch ex As Exception
            GPFSwriteErrfile("處理檔案:" + FileNo + ":發生錯誤,訊息:" + ex.Message)
        End Try
        TimerGPFSCheckTSMFileStatus.Enabled = True
    End Sub

    Private Sub MenuItemExit_Click(sender As System.Object, e As System.EventArgs) Handles MenuItemExit.Click
        Close()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub MenuItemSetting_Click(sender As System.Object, e As System.EventArgs) Handles MenuItemSetting.Click

    End Sub

    Function frame2timecode(ByVal frames As Integer) As String
        Dim HH, MM, SS, FF, itmp, M1 As Integer
        HH = Math.Floor(frames / 107892)
        itmp = frames Mod 107892

        MM = Math.Floor(itmp / 17982) * 10
        itmp = itmp Mod 17982
        If itmp > 2 Then
            M1 = Math.Floor((itmp - 2) / 1798)
            itmp = (itmp - 2) Mod 1798
            itmp = itmp + 2
        Else
            M1 = 0
        End If
        MM = MM + M1
        SS = Math.Floor((itmp) / 30)
        FF = itmp Mod 30
        Return String.Format("{0:00}", HH) + ":" + String.Format("{0:00}", MM) + ":" + String.Format("{0:00}", SS) + ":" + String.Format("{0:00}", FF)
    End Function

    Function timecode2frame(ByVal TIMECODE As String) As Integer
        Dim HH, MM, SS, FF, itmp, M1 As Integer
        HH = Int32.Parse(TIMECODE.Substring(0, 2))
        MM = Int32.Parse(TIMECODE.Substring(3, 1))
        M1 = Int32.Parse(TIMECODE.Substring(4, 1))
        SS = Int32.Parse(TIMECODE.Substring(6, 2))
        FF = Int32.Parse(TIMECODE.Substring(9, 2))
        itmp = HH * 107892 + MM * 17982
        If M1 > 0 Then
            itmp = itmp + M1 * 1798
        End If
        itmp = itmp + SS * 30
        itmp = itmp + FF
        Return itmp
    End Function


    Function Timecode2LouthTimecode(ByVal TIMECODE As String) As Integer
        Dim RTime As Integer
        RTime = Int32.Parse(TIMECODE.Substring(0, 1)) * 16 * 16 * 16 * 16 * 16 * 16 * 16
        RTime = RTime + Int32.Parse(TIMECODE.Substring(1, 1)) * 16 * 16 * 16 * 16 * 16 * 16
        RTime = RTime + Int32.Parse(TIMECODE.Substring(3, 1)) * 16 * 16 * 16 * 16 * 16
        RTime = RTime + Int32.Parse(TIMECODE.Substring(4, 1)) * 16 * 16 * 16 * 16
        RTime = RTime + Int32.Parse(TIMECODE.Substring(6, 1)) * 16 * 16 * 16
        RTime = RTime + Int32.Parse(TIMECODE.Substring(7, 1)) * 16 * 16
        RTime = RTime + Int32.Parse(TIMECODE.Substring(9, 1)) * 16
        RTime = RTime + Int32.Parse(TIMECODE.Substring(10, 1))
        Return RTime
    End Function
    Function ReplaceXML(ByVal XMLString As String) As String
        XMLString = XMLString.Replace("&", "&amp;")
        XMLString = XMLString.Replace("'", "&apos;")
        XMLString = XMLString.Replace("""", "&quot;")
        XMLString = XMLString.Replace(">", "&gt;")
        XMLString = XMLString.Replace("<", "&lt;")
        Return XMLString
    End Function
    '取得輸入字串前幾個字元byte數,中文字算2byte
    Function GetSubString(ByVal str As String, ByVal getStrLength As Integer) As String
        Dim StrLength As Integer = 0
        Dim StrSubLength As Integer = 0
        Dim c As Char
        For Each c In str
            If Asc(c) > 33 And Asc(c) < 126 Then
                If StrLength + 1 > getStrLength Then
                    Return str.Substring(0, StrSubLength)
                End If
                StrLength = StrLength + 1
                StrSubLength = StrSubLength + 1
            Else
                If StrLength + 2 > getStrLength Then
                    Return str.Substring(0, StrSubLength)
                End If
                StrLength = StrLength + 2
                StrSubLength = StrSubLength + 1
            End If
        Next

        Return str.Substring(0, StrSubLength)
    End Function

    Public Sub AddInsertLouthDBFile(ByVal videoid As String)


        'Dim mycommand As New SqlCommand
        Dim sqlconn As New SqlConnection(SqlConnString)
        Dim i As Integer
        Try

            sqlconn.Open()
            'mycommand.Connection = sqlconn
            If videoid.Length = 6 Then
                videoid = "00" + videoid
            End If

            Dim da As SqlDataAdapter = New SqlDataAdapter("SP_Q_GET_FILE_SEG_BY_VIDEOID_MIX", sqlconn)
            Dim dt As New DataTable()
            'LouthDBwritefile("開始查詢檔案[" + videoid + " ]的段落")

            Dim para As SqlParameter = New SqlParameter("@FSVIDEO_ID", videoid)

            da.SelectCommand.Parameters.Add(para)
            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.Fill(dt)

            'If dt.Rows.Count > 0 Then
            'distinct A.FSPROG_ID as FSID,A.FNEPISODE,D.FSFILE_NO,D.FSVIDEO_ID,B.FCFILE_STATUS,C.FCCHECK_STATUS,D.FCSTATUS
            Dim j As Integer
            Dim k As Integer
            Dim flag As Boolean
            Dim LouthDBXML As String = ""
            LouthDBwritefile("開始寫入檔案[" + videoid + " ]的段落")
            LouthDBXML = "<Datas>" & Chr(13) & Chr(10)

            Dim DateString As String = ""
            DateString = DateString + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, "0") + DateTime.Now.Day.ToString().PadLeft(2, "0") + "-" + DateTime.Now.Hour.ToString().PadLeft(2, "0") + DateTime.Now.Minute.ToString().PadLeft(2, "0") + DateTime.Now.Second.ToString().PadLeft(2, "0")
            Dim InsDateString As String = ""
            InsDateString = InsDateString + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString().PadLeft(2, "0") + "-" + DateTime.Now.Day.ToString().PadLeft(2, "0") + " " + DateTime.Now.Hour.ToString().PadLeft(2, "0") + ":" + DateTime.Now.Minute.ToString().PadLeft(2, "0") + ":" + DateTime.Now.Second.ToString().PadLeft(2, "0") + "." + DateTime.Now.Millisecond.ToString().PadLeft(3, "0")
            For i = 0 To dt.Rows.Count - 1
                'select top 100 A.FSFILE_NO,A.FSVIDEO_PROG,A.FSTYPE,B.FNSEG_ID,B.FSBEG_TIMECODE,B.FSEND_TIMECODE 
                If dt.Rows(i)("FSTYPE").ToString() = "G" Then '節目
                    If dt.Rows(i)("FNSEG_ID").ToString() = "1" Or dt.Rows(i)("FNSEG_ID").ToString() = "01" Then
                        '第一段要先加入ASDB,每段要加ASSEG
                        'PendingLogStr = PendingLogStr & Now.ToString & " : " & MessageStr & Chr(13) & Chr(10)
                        Dim Dur As Integer = 0
                        For k = 0 To dt.Rows.Count - 1
                            Dur = Dur + timecode2frame(dt.Rows(k)("FSEND_TIMECODE").ToString()) - timecode2frame(dt.Rows(k)("FSBEG_TIMECODE").ToString())
                        Next
                        LouthDBXML = LouthDBXML & "<Data>" & Chr(13) & Chr(10)
                        LouthDBXML = LouthDBXML & "<TableName>ASDB</TableName>" & Chr(13) & Chr(10)
                        LouthDBXML = LouthDBXML & "<Type>m</Type>" & Chr(13) & Chr(10)
                        LouthDBXML = LouthDBXML & "<Identifier>" + videoid.Substring(2, 6) + "</Identifier>" & Chr(13) & Chr(10)
                        'LouthDBXML = LouthDBXML & "<Identifier>" + dt.Rows(i)("FSVIDEO_PROG").ToString() + "</Identifier>" & Chr(13) & Chr(10)
                        If dt.Rows(i)("FNEPISODE").ToString() <> "" Then
                            LouthDBXML = LouthDBXML & "<Title>" + ReplaceXML(GetSubString(dt.Rows(i)("FSNAME").ToString(), 16 - (dt.Rows(i)("FNEPISODE").ToString().Length + 1)) + "#" + dt.Rows(i)("FNEPISODE").ToString()) + "</Title>" & Chr(13) & Chr(10)
                        Else
                            LouthDBXML = LouthDBXML & "<Title>" + ReplaceXML(GetSubString(dt.Rows(i)("FSNAME").ToString(), 16)) + "</Title>" & Chr(13) & Chr(10)
                        End If
                        LouthDBXML = LouthDBXML & "<Operator>" + "MAM" + "</Operator>" & Chr(13) & Chr(10)
                        LouthDBXML = LouthDBXML & "<StartOfMessage>" + Timecode2LouthTimecode(dt.Rows(i)("FSBEG_TIMECODE").ToString()).ToString() + "</StartOfMessage>" & Chr(13) & Chr(10)
                        LouthDBXML = LouthDBXML & "<Duration>" + Timecode2LouthTimecode(frame2timecode(Dur)).ToString() + "</Duration>" & Chr(13) & Chr(10)
                        LouthDBXML = LouthDBXML & "<LabelType>" + "0" + "</LabelType>" & Chr(13) & Chr(10)
                        LouthDBXML = LouthDBXML & "<VideoFormat>" + "4" + "</VideoFormat>" & Chr(13) & Chr(10)
                        LouthDBXML = LouthDBXML & "<AudioFormat>" + "1" + "</AudioFormat>" & Chr(13) & Chr(10)

                        LouthDBXML = LouthDBXML & "<VideoQuality>" + "0" + "</VideoQuality>" & Chr(13) & Chr(10)
                        LouthDBXML = LouthDBXML & "<MadeDateTime>" + InsDateString + "</MadeDateTime>" & Chr(13) & Chr(10)
                        LouthDBXML = LouthDBXML & "<AirDateTime>" + InsDateString + "</AirDateTime>" & Chr(13) & Chr(10)
                        LouthDBXML = LouthDBXML & "<PurgeDateTime>" + InsDateString + "</PurgeDateTime>" & Chr(13) & Chr(10)
                        LouthDBXML = LouthDBXML & "<UserString>" + "" + "</UserString>" & Chr(13) & Chr(10)
                        LouthDBXML = LouthDBXML & "</Data>" & Chr(13) & Chr(10)
                    End If
                    '第一段寫入ASDB結束,開始寫入ASSEG
                    LouthDBXML = LouthDBXML & "<Data>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<TableName>ASSEG</TableName>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<Type>m</Type>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<SegNum>" + Int32.Parse(dt.Rows(i)("FNSEG_ID").ToString()).ToString() + "</SegNum>" & Chr(13) & Chr(10)
                    'LouthDBXML = LouthDBXML & "<Identifier>" + dt.Rows(i)("FSVIDEO_PROG").ToString() + "</Identifier>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<Identifier>" + videoid.Substring(2, 6) + "</Identifier>" & Chr(13) & Chr(10)
                    If dt.Rows(i)("FNEPISODE").ToString() <> "" Then
                        LouthDBXML = LouthDBXML & "<Title>" + ReplaceXML(GetSubString(dt.Rows(i)("FSNAME").ToString(), 16 - (dt.Rows(i)("FNEPISODE").ToString().Length + 1)) + "#" + dt.Rows(i)("FNEPISODE").ToString()) + "</Title>" & Chr(13) & Chr(10)
                    Else
                        LouthDBXML = LouthDBXML & "<Title>" + ReplaceXML(GetSubString(dt.Rows(i)("FSNAME").ToString(), 16)) + "</Title>" & Chr(13) & Chr(10)
                    End If
                    LouthDBXML = LouthDBXML & "<StartOfMessage>" + Timecode2LouthTimecode(dt.Rows(i)("FSBEG_TIMECODE").ToString()).ToString() + "</StartOfMessage>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<Duration>" + Timecode2LouthTimecode(frame2timecode(timecode2frame(dt.Rows(i)("FSEND_TIMECODE").ToString()) - timecode2frame(dt.Rows(i)("FSBEG_TIMECODE").ToString()))).ToString() + "</Duration>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<NoteDateTime>" + InsDateString + "</NoteDateTime>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "</Data>" & Chr(13) & Chr(10)

                ElseIf dt.Rows(i)("FSTYPE").ToString() = "P" Then '短帶
                    LouthDBXML = LouthDBXML & "<Data>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<TableName>ASDB</TableName>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<Type>s</Type>" & Chr(13) & Chr(10)
                    'LouthDBXML = LouthDBXML & "<Identifier>" + dt.Rows(i)("FSVIDEO_PROG").ToString() + "</Identifier>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<Identifier>" + videoid.Substring(2, 6) + "</Identifier>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<Title>" + ReplaceXML(GetSubString(dt.Rows(i)("FSNAME").ToString(), 16)) + "</Title>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<Operator>" + "MAM" + "</Operator>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<StartOfMessage>" + Timecode2LouthTimecode(dt.Rows(i)("FSBEG_TIMECODE").ToString()).ToString() + "</StartOfMessage>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<Duration>" + Timecode2LouthTimecode(frame2timecode(timecode2frame(dt.Rows(i)("FSEND_TIMECODE").ToString()) - timecode2frame(dt.Rows(i)("FSBEG_TIMECODE").ToString()))).ToString() + "</Duration>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<LabelType>" + "0" + "</LabelType>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<VideoFormat>" + "4" + "</VideoFormat>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<AudioFormat>" + "1" + "</AudioFormat>" & Chr(13) & Chr(10)

                    LouthDBXML = LouthDBXML & "<VideoQuality>" + "0" + "</VideoQuality>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<MadeDateTime>" + InsDateString + "</MadeDateTime>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<AirDateTime>" + InsDateString + "</AirDateTime>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<PurgeDateTime>" + InsDateString + "</PurgeDateTime>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "<UserString>" + "" + "</UserString>" & Chr(13) & Chr(10)
                    LouthDBXML = LouthDBXML & "</Data>" & Chr(13) & Chr(10)
                End If
            Next
            LouthDBXML = LouthDBXML & "</Datas>"
            sqlconn.Close()
            sqlconn.Dispose()
            'GC.Collect()
            'GC.WaitForPendingFinalizers()

            If System.IO.File.Exists(LouthXmlPath & DateString & ".xml") = False Then
                My.Computer.FileSystem.WriteAllText(LouthXmlPath & DateString & ".xml", LouthDBXML, False)
                My.Computer.FileSystem.WriteAllText(LouthXmlPath & DateString & ".xml.pgmok", "", False)
            Else
                DateString = DateString + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, "0") + DateTime.Now.Day.ToString().PadLeft(2, "0") + "-" + DateTime.Now.Hour.ToString().PadLeft(2, "0") + DateTime.Now.Minute.ToString().PadLeft(2, "0") + DateTime.Now.Second.ToString().PadLeft(2, "0")
                My.Computer.FileSystem.WriteAllText(LouthXmlPath & DateString & ".xml", LouthDBXML, False)
                My.Computer.FileSystem.WriteAllText(LouthXmlPath & DateString & ".xml.pgmok", "", False)
            End If
            LouthDBwritefile("寫入檔案[" + videoid + " ]的段落完畢")

        Catch ex As Exception

            LouthDBwriteErrfile("寫入檔案[" + videoid + " ]的段落失敗:訊息:" & ex.Message)
            'For k = DataGridViewWatchTempZone.RowCount - 1 To 0 Step -1
            '    If DataGridViewWatchTempZone.Rows(k).Cells(5).Value = SourceFileName Then
            '        writeErrfile("修改[" & WorkVideoID & "]狀態失敗:訊息:" & ex.Message)
            '        DataGridViewWatchTempZone.Rows(k).Cells(0).Value = "尚未開始"
            '    End If
            'Next


        End Try
    End Sub

    Private Sub TimerAutoDeleteFile_Tick(sender As System.Object, e As System.EventArgs) Handles TimerAutoDeleteFile.Tick
        TimerAutoDeleteFile.Enabled = False
        Dim mycommand As New SqlCommand
        Dim sqlconn As New SqlConnection(SqlConnString)
        Dim i As Integer
        Try

            sqlconn.Open()
            mycommand.Connection = sqlconn
            '查詢已審核完成且在14天內要播出的檔案,且未轉檔
            Dim da As SqlDataAdapter = New SqlDataAdapter("SP_Q_GET_PLAY_VIDEOID_LIST", sqlconn)
            Dim dt As New DataTable()

            'Dim para As SqlParameter=New SqlParameter("@FDDATE", TemArrPromoList.FSPROG_ID);

            'da.SelectCommand.Parameters.Add(para);
            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.Fill(dt)

            'If dt.Rows.Count > 0 Then
            'distinct A.FSPROG_ID as FSID,A.FNEPISODE,D.FSFILE_NO,D.FSVIDEO_ID,B.FCFILE_STATUS,C.FCCHECK_STATUS,D.FCSTATUS
            If System.IO.Directory.Exists(CompletePath) = True Then
                Dim oFS As New DirectoryInfo(CompletePath)
                Dim oFile As FileInfo
                For Each oFile In oFS.GetFiles()
                    If oFile.Extension.ToLower = ".mxf" Then
                        Dim flag As Boolean = False
                        For i = 0 To dt.Rows.Count - 1
                            If dt.Rows(i)("FSVIDEO_ID").ToString() + ".MXF" = oFile.Name.ToUpper() Then
                                flag = True
                            End If
                        Next
                        If flag = False Then '不在14天的清單內,需要刪除檔案
                            Delwritefile("刪除已審區檔案:" + oFile.Name + "")
                            If System.IO.File.Exists(CompletePath + oFile.Name) = True Then
                                System.IO.File.Delete(CompletePath + oFile.Name)
                            End If
                            If System.IO.File.Exists(CompletePath + oFile.Name + ".ok") = True Then
                                System.IO.File.Delete(CompletePath + oFile.Name + ".ok")
                            End If


                        End If
                    End If
                Next
            End If



            sqlconn.Close()
            sqlconn.Dispose()
            'GC.Collect()
            'GC.WaitForPendingFinalizers()


            'End If
        Catch ex As Exception

            sqlconn.Close()
            DelwriteErrfile("刪除已審區檔案發生錯誤,訊息:" + ex.Message)
            Exit Sub
        End Try
        TimerAutoDeleteFile.Enabled = True

    End Sub

    Private Sub Button1_Click_1(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub TimerGCClear_Tick(sender As System.Object, e As System.EventArgs) Handles TimerGCClear.Tick
        TimerGCClear.Enabled = False
        GC.Collect()
        'GC.WaitForPendingFinalizers()
        TimerGCClear.Enabled = True
    End Sub
End Class
