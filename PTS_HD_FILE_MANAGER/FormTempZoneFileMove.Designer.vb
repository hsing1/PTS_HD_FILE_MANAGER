﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormTempZoneFileMove
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItemSetting = New System.Windows.Forms.MenuItem()
        Me.MenuItemStop = New System.Windows.Forms.MenuItem()
        Me.MenuItemStart = New System.Windows.Forms.MenuItem()
        Me.MenuItemExit = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.TimerRefresh = New System.Windows.Forms.Timer(Me.components)
        Me.TimerUpload = New System.Windows.Forms.Timer(Me.components)
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.DataGridViewWatchTempZone = New System.Windows.Forms.DataGridView()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.DataGridViewPENDINGList = New System.Windows.Forms.DataGridView()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.DataGridViewGPFS = New System.Windows.Forms.DataGridView()
        Me.TimerPENDINGRefresh = New System.Windows.Forms.Timer(Me.components)
        Me.TimerPendingUpload = New System.Windows.Forms.Timer(Me.components)
        Me.TimerWriteLog = New System.Windows.Forms.Timer(Me.components)
        Me.TimerGPFSRefresh = New System.Windows.Forms.Timer(Me.components)
        Me.TimerGPFSUpload = New System.Windows.Forms.Timer(Me.components)
        Me.TimerGPFSCheckTSMFileStatus = New System.Windows.Forms.Timer(Me.components)
        Me.TimerAutoDeleteFile = New System.Windows.Forms.Timer(Me.components)
        Me.TimerGCClear = New System.Windows.Forms.Timer(Me.components)
        Me.ColumnStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnBroadID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnFileNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnVID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnSourcePath = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnDescPath = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnPlayTime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.DataGridViewWatchTempZone, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.DataGridViewPENDINGList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.DataGridViewGPFS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemSetting, Me.MenuItemStop, Me.MenuItemStart, Me.MenuItemExit, Me.MenuItem1})
        '
        'MenuItemSetting
        '
        Me.MenuItemSetting.Index = 0
        Me.MenuItemSetting.Text = "設定(&S)"
        Me.MenuItemSetting.Visible = False
        '
        'MenuItemStop
        '
        Me.MenuItemStop.Index = 1
        Me.MenuItemStop.Text = "暫停(&P)"
        '
        'MenuItemStart
        '
        Me.MenuItemStart.Enabled = False
        Me.MenuItemStart.Index = 2
        Me.MenuItemStart.Text = "啟動(&G)"
        '
        'MenuItemExit
        '
        Me.MenuItemExit.Index = 3
        Me.MenuItemExit.Text = "離開(&X)"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 4
        Me.MenuItem1.Text = "aa"
        Me.MenuItem1.Visible = False
        '
        'TimerRefresh
        '
        Me.TimerRefresh.Interval = 60000
        '
        'TimerUpload
        '
        Me.TimerUpload.Interval = 60000
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Font = New System.Drawing.Font("微軟正黑體", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1339, 530)
        Me.TabControl1.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.DataGridViewWatchTempZone)
        Me.TabPage1.Location = New System.Drawing.Point(4, 29)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(1331, 497)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "暫存區-->待審區"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'DataGridViewWatchTempZone
        '
        Me.DataGridViewWatchTempZone.AllowUserToAddRows = False
        Me.DataGridViewWatchTempZone.AllowUserToDeleteRows = False
        Me.DataGridViewWatchTempZone.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewWatchTempZone.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColumnStatus, Me.ColumnBroadID, Me.ColumnType, Me.ColumnFileNo, Me.ColumnVID, Me.ColumnSourcePath, Me.ColumnDescPath, Me.ColumnPlayTime})
        Me.DataGridViewWatchTempZone.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridViewWatchTempZone.Location = New System.Drawing.Point(0, 0)
        Me.DataGridViewWatchTempZone.Name = "DataGridViewWatchTempZone"
        Me.DataGridViewWatchTempZone.ReadOnly = True
        Me.DataGridViewWatchTempZone.RowTemplate.Height = 24
        Me.DataGridViewWatchTempZone.Size = New System.Drawing.Size(1331, 497)
        Me.DataGridViewWatchTempZone.TabIndex = 0
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.DataGridViewPENDINGList)
        Me.TabPage2.Location = New System.Drawing.Point(4, 29)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(1331, 497)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "待審區-->待播區"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'DataGridViewPENDINGList
        '
        Me.DataGridViewPENDINGList.AllowUserToAddRows = False
        Me.DataGridViewPENDINGList.AllowUserToDeleteRows = False
        Me.DataGridViewPENDINGList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewPENDINGList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn15})
        Me.DataGridViewPENDINGList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridViewPENDINGList.Location = New System.Drawing.Point(0, 0)
        Me.DataGridViewPENDINGList.Name = "DataGridViewPENDINGList"
        Me.DataGridViewPENDINGList.ReadOnly = True
        Me.DataGridViewPENDINGList.RowTemplate.Height = 24
        Me.DataGridViewPENDINGList.Size = New System.Drawing.Size(1331, 497)
        Me.DataGridViewPENDINGList.TabIndex = 1
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.DataGridViewGPFS)
        Me.TabPage3.Location = New System.Drawing.Point(4, 29)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1331, 497)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "GPFS-->待播區"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'DataGridViewGPFS
        '
        Me.DataGridViewGPFS.AllowUserToAddRows = False
        Me.DataGridViewGPFS.AllowUserToDeleteRows = False
        Me.DataGridViewGPFS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewGPFS.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn16})
        Me.DataGridViewGPFS.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridViewGPFS.Location = New System.Drawing.Point(0, 0)
        Me.DataGridViewGPFS.Name = "DataGridViewGPFS"
        Me.DataGridViewGPFS.ReadOnly = True
        Me.DataGridViewGPFS.RowTemplate.Height = 24
        Me.DataGridViewGPFS.Size = New System.Drawing.Size(1331, 497)
        Me.DataGridViewGPFS.TabIndex = 2
        '
        'TimerPENDINGRefresh
        '
        Me.TimerPENDINGRefresh.Interval = 60000
        '
        'TimerPendingUpload
        '
        Me.TimerPendingUpload.Interval = 60000
        '
        'TimerWriteLog
        '
        Me.TimerWriteLog.Interval = 5000
        '
        'TimerGPFSRefresh
        '
        Me.TimerGPFSRefresh.Interval = 60000
        '
        'TimerGPFSUpload
        '
        Me.TimerGPFSUpload.Interval = 60000
        '
        'TimerGPFSCheckTSMFileStatus
        '
        Me.TimerGPFSCheckTSMFileStatus.Interval = 60000
        '
        'TimerAutoDeleteFile
        '
        Me.TimerAutoDeleteFile.Interval = 60000
        '
        'TimerGCClear
        '
        Me.TimerGCClear.Interval = 300000
        '
        'ColumnStatus
        '
        Me.ColumnStatus.HeaderText = "處理狀態"
        Me.ColumnStatus.Name = "ColumnStatus"
        Me.ColumnStatus.ReadOnly = True
        Me.ColumnStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'ColumnBroadID
        '
        Me.ColumnBroadID.HeaderText = "單號"
        Me.ColumnBroadID.Name = "ColumnBroadID"
        Me.ColumnBroadID.ReadOnly = True
        Me.ColumnBroadID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'ColumnType
        '
        Me.ColumnType.HeaderText = "類型"
        Me.ColumnType.Name = "ColumnType"
        Me.ColumnType.ReadOnly = True
        Me.ColumnType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'ColumnFileNo
        '
        Me.ColumnFileNo.HeaderText = "檔案編號"
        Me.ColumnFileNo.Name = "ColumnFileNo"
        Me.ColumnFileNo.ReadOnly = True
        Me.ColumnFileNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'ColumnVID
        '
        Me.ColumnVID.HeaderText = "VideoID"
        Me.ColumnVID.Name = "ColumnVID"
        Me.ColumnVID.ReadOnly = True
        Me.ColumnVID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'ColumnSourcePath
        '
        Me.ColumnSourcePath.HeaderText = "來源路徑"
        Me.ColumnSourcePath.Name = "ColumnSourcePath"
        Me.ColumnSourcePath.ReadOnly = True
        Me.ColumnSourcePath.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ColumnSourcePath.Width = 300
        '
        'ColumnDescPath
        '
        Me.ColumnDescPath.HeaderText = "目的位置"
        Me.ColumnDescPath.Name = "ColumnDescPath"
        Me.ColumnDescPath.ReadOnly = True
        Me.ColumnDescPath.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ColumnDescPath.Width = 300
        '
        'ColumnPlayTime
        '
        Me.ColumnPlayTime.HeaderText = "播出時間"
        Me.ColumnPlayTime.Name = "ColumnPlayTime"
        Me.ColumnPlayTime.ReadOnly = True
        Me.ColumnPlayTime.Width = 150
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "處理狀態"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "單號"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "類型"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "檔案編號"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "VideoID"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "來源路徑"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Width = 300
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "目的位置"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 300
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "播出時間"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.Width = 150
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "處理狀態"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "單號"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "類型"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "檔案編號"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "VideoID"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "來源路徑"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn13.Width = 300
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "目的位置"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn14.Width = 300
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "播出時間"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.Width = 150
        '
        'FormTempZoneFileMove
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1339, 530)
        Me.Controls.Add(Me.TabControl1)
        Me.Menu = Me.MainMenu1
        Me.Name = "FormTempZoneFileMove"
        Me.Text = "HD檔案處理"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.DataGridViewWatchTempZone, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.DataGridViewPENDINGList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        CType(Me.DataGridViewGPFS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItemSetting As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemStop As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemStart As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemExit As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents TimerRefresh As System.Windows.Forms.Timer
    Friend WithEvents TimerUpload As System.Windows.Forms.Timer
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents DataGridViewWatchTempZone As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents DataGridViewPENDINGList As System.Windows.Forms.DataGridView
    Friend WithEvents TimerPENDINGRefresh As System.Windows.Forms.Timer
    Friend WithEvents TimerPendingUpload As System.Windows.Forms.Timer
    Friend WithEvents TimerWriteLog As System.Windows.Forms.Timer
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents DataGridViewGPFS As System.Windows.Forms.DataGridView
    Friend WithEvents TimerGPFSRefresh As System.Windows.Forms.Timer
    Friend WithEvents TimerGPFSUpload As System.Windows.Forms.Timer
    Friend WithEvents TimerGPFSCheckTSMFileStatus As System.Windows.Forms.Timer
    Friend WithEvents TimerAutoDeleteFile As System.Windows.Forms.Timer
    Friend WithEvents TimerGCClear As System.Windows.Forms.Timer
    Friend WithEvents ColumnStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnBroadID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnFileNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnVID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnSourcePath As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnDescPath As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnPlayTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
